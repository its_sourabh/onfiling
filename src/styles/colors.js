/*
 * Color can communicate the meaning of different UI elements. For example, a weather app may display colors indicating current weather conditions, and a navigation app may display color showing traffic conditions, with roads colored red or green
 */

export const black = '#000000';
export const white = '#ffffff';
export const primary = '#ffb600';
export const accent = '#8BC34A';
export const background = '#ffffff';
export const error = '#B00020';
