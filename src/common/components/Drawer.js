import React, { Component } from 'react';
import {
  Text, View, ScrollView, Dimensions, Image
} from 'react-native';
import { DrawerItems, SafeAreaView, NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import Layout from 'app/src/styles/layout';
import Typography from 'app/src/styles/typography';
import Router from 'app/src/routes/Router';
import * as UserDucks from 'app/src/accounts/ducks/user';


class CustomDrawer extends React.Component {
  constructor() {
    super();
    this.onItemPress = this.onItemPress.bind(this);
  }

  async onItemPress(router) {
    const routeName = router.route.routeName;
    if (routeName == 'Logout') {
      Router.toggleDrawer();
      Router.navigate('Onboarding');
      return;
    }
    const navigateAction = NavigationActions.navigate({
      routeName
    });
    this.props.navigation.dispatch(navigateAction);
  }

  render() {
    return (
      <SafeAreaView
        style={Layout.safeArea}
      >
        <View style={Layout.selfCenter}>
          <Text style={Typography.headline}>
            Granite
          </Text>
        </View>
        <View style={[Layout.selfCenter,Layout.inputFieldWrapper]}>
          <Text style={Typography.title}>
            Welcome User
          </Text>
        </View>
        <ScrollView>
          <DrawerItems {...this.props} onItemPress={this.onItemPress} />
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  profile: UserDucks.profile(state)
});

const mapDispatchToProps = dispatch => ({
  dispatch
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomDrawer);
