import React, { Component } from 'react';
import { View, Image} from 'react-native';
import Layout from 'app/src/styles/layout';
import Router from 'app/src/routes/Router';
export default class ImageGrid extends Component {
	constructor(props) {
    super(props); 
    console.log('new props',props);        
  }
	render() {
    console.log('imagesArray',this.props.imagesArray.length);
		return (
    <View style={{height:200}}>
			{
        this.props.imagesArray.length == 4 && 
          <View style={[Layout.column,{alignItems: 'center'}]}>
            <View style={[Layout.row,{flex:1}]}>
              <View style = {{flex:0.5}}> 
                <Image
                  style={{width:"100%",height:"100%",resizeMode: 'cover'}}
                  source={{uri: this.props.imagesArray[0]}}
                />   
              </View>
              <View style = {{flex:0.5}}> 
                <Image
                  style={{width:"100%",height:"100%",resizeMode: 'cover'}}
                  source={{uri: this.props.imagesArray[1]}}
                />   
              </View>
            </View>   
            <View style={[Layout.row,{flex:1}]}>
              <View style = {{flex:0.5}}> 
                <Image
                  style={{width:"100%",height:"100%",resizeMode: 'cover'}}
                  source={{uri: this.props.imagesArray[2]}}
                />   
              </View>
              <View style = {{flex:0.5}}> 
                <Image
                  style={{width:"100%",height:"100%",resizeMode: 'cover'}}
                  source={{uri: this.props.imagesArray[3]}}
                />   
              </View>
            </View>  
          </View>
      }
      { 
        this.props.imagesArray.length == 3 &&
          <View style={[Layout.column]}>
            <View style={[Layout.row,{flex:1}]}>
              <View style = {{flex:0.5}}> 
                <Image
                  style={{width:"100%",height:"100%",resizeMode: 'cover'}}
                  source={{uri: this.props.imagesArray[0]}}
                />   
              </View>
              <View style = {{flex:0.5}}> 
                <Image
                  style={{width:"100%",height:"100%",resizeMode: 'cover'}}
                  source={{uri: this.props.imagesArray[1]}}
                />   
              </View>
            </View>   
            <View style = {{flex:1}}>
               <Image
                  style={{width:"100%",height:"100%",resizeMode: 'cover'}}
                  source={{uri: this.props.imagesArray[2]}}
                />   
            </View>     
          </View>
      }
      {
        this.props.imagesArray.length == 2 &&
          <View style={[Layout.row,{flex:1}]}> 
            <View style = {{flex:1}}>   
              <Image
                style={{width:"100%",height:"100%",resizeMode: 'cover'}}
                source={{uri: this.props.imagesArray[0]}}
              />
            </View>
            <View style = {{flex:1}}> 
                <Image
                  style={{width:"100%",height:"100%",resizeMode: 'cover'}}
                  source={{uri:this.props.imagesArray[1]}}
                />   
              </View>     
          </View>
      }
      {
        this.props.imagesArray.length == 1 &&
          <View style = {{flex:1}}> 
            <Image
              style={{width:"100%",height:"100%",resizeMode: 'cover'}}
              source={{uri: this.props.imagesArray[0]}}
            />  
          </View>         
      }
    </View>  
		);
	}
}
