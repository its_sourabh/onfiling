import React, { PureComponent } from 'react';
import { Alert } from 'react-native';
import { Observable } from 'rxjs';

import Geolocation from 'react-native-geolocation-service';
import Permissions from 'react-native-permissions';


class LocationTracker extends PureComponent {

  constructor(props) {
    super(props);
    this.locationPermission = 'undetermined';
    this.currentLocation = null;
    this.subscription = null;

    this._getLocation = this._getLocation.bind(this);
    this._watchLocation = this._watchLocation.bind(this);
    this._alertForPhotosPermission = this._alertForPhotosPermission.bind(this);
    this._requestPermission = this._requestPermission.bind(this);
    this._listenLocationChange = this._listenLocationChange.bind(this);
  }

  componentDidMount() {
    const { watch } = this.props;
    Permissions.check('location').then(response => {
      // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
      this.setState({ photoPermission: response });
      if(response == 'undetermined') {
        this._alertForPhotosPermission();
      }
      else if(response == 'authorized') {
        // We recieved the permission, start watching it
        this._getLocation();
        if(watch) {
          this._listenLocationChange();
        }
      }
    });
  }

  componentWillUnmount() {
    if(this.subscription != null) {
      this.subscription.unsubscribe();
      this.subscription = null;
    }
  }

  _getLocation() {
    const { didLocationChange } = this.props;
    Geolocation.getCurrentPosition(
      (position) => {
        if (didLocationChange) {
          didLocationChange(position);
        }
      },
      (error) => {
        // Code can be - PERMISSION_DENIED | POSITION_UNAVAILABLE | TIMEOUT | PLAY_SERVICE_NOT_AVAILABLE | SETTINGS_NOT_SATISFIED | INTERNAL_ERROR
        console.log(error.code, error.message);
      },
      { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    );
  }

  _listenLocationChange() {
    const { didLocationChange } = this.props;
    const source = this._watchLocation();
    this.subscription = source.subscribe(
      position => {
        console.log(`Next: ${position.coords.latitude}, ${position.coords.longitude}`)
        if (didLocationChange) {
          didLocationChange(position);
        }
      },
      err => {
        // Code can be - PERMISSION_DENIED | POSITION_UNAVAILABLE | TIMEOUT | PLAY_SERVICE_NOT_AVAILABLE | SETTINGS_NOT_SATISFIED | INTERNAL_ERROR
        console.log('Error: ' + err.message);
      },
      () => console.log('Completed')
    );
  }

  _watchLocation() {
    return Observable.create(observer => {
      const watchId = Geolocation.watchPosition(
        function successHandler (loc) {
          observer.next(loc);
        }, 
        function errorHandler (err) {
          observer.error(err);
        }, 
        { enableHighAccuracy: true, interval: 1000, distanceFilter: 10 }  
      );

      return () => {
        Geolocation.clearWatch(watchId);
      };
    });
  }

  // Request permission to access photos
  _requestPermission = () => {
    const { watch } = this.props;
    Permissions.request('location').then(response => {
      this.locationPermission = response;
      // We recieved the permission, start watching it
      this._getLocation();
      if(watch) {
        this._listenLocationChange();
      }
    })
  }

  // This is a common pattern when asking for permissions.
  // iOS only gives you once chance to show the permission dialog,
  // after which the user needs to manually enable them from settings.
  // The idea here is to explain why we need access and determine if
  // the user will say no, so that we don't blow our one chance.
  // If the user already denied access, we can ask them to enable it from settings.
  _alertForPhotosPermission() {
    Alert.alert(
      'Can we access your location?',
      'We need access your location so you can enable Map features.',
      [
        {
          text: 'No way',
          onPress: () => console.log('Permission denied'),
          style: 'cancel',
        },
        this.locationPermission == 'undetermined'
          ? { text: 'OK', onPress: this._requestPermission }
          : { text: 'Open Settings', onPress: Permissions.openSettings },
      ],
    );
  }

  render() {
    return null;
  }
}

export default LocationTracker;
