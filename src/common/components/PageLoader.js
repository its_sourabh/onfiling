import React, { Component } from 'react'
import {
  ActivityIndicator,
  SafeAreaView,
  StyleSheet
} from 'react-native'
export default class PageLoader extends Component {
  render() {
    return (
      <SafeAreaView style={[styles.safeArea,styles.contentCenter]}>
        <ActivityIndicator size="large" />   
      </SafeAreaView>
    )
  }
}
const styles = StyleSheet.create({
  safeArea: {
      flex: 1,
      backgroundColor: 'white'
  },
  contentCenter: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center'
},  
});


