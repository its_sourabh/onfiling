import Svg,{Rect} from 'react-native-svg';
import React from 'react';
import { View, StyleSheet } from 'react-native';
import Layout from 'app/src/styles/layout';
import ContentLoader from 'rn-content-loader';
export default class App extends React.Component {
  constructor(props) {
      super(props);      
  }
  render() {
    console.log('props for ContentLoader',this.props);
    console.log('loaderData',this.props.loaderData);  
    this.props.loaderData.map((item) => {
    	console.log('x',item.x);  
	})
    return (
      <View style={Layout.safeArea}>  
        <ContentLoader  width='100%' height='100%'>    
	    	{this.props.loaderData.map((item) => 
			    <Rect x={item.x} y={item.y} rx={item.rx} ry={item.ry} width={item.width} height={item.height}/>
	    	)}
        </ContentLoader>        
      </View>
    );
  }
}
const styles= StyleSheet.create({
   contentCenter: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
});