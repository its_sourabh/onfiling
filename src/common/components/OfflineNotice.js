import React, { Component } from 'react';
import { NetInfo } from 'react-native';

import { fromEvent } from 'rxjs';
import { throttleTime, map } from 'rxjs/operators';
import Toast from 'react-native-root-toast';

import Theme from 'app/src/styles/theme';

class OfflineNotice extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isOffline: false
    };
    const { onToggle = () => {} } = props;
    this.subscription = fromEvent(NetInfo, 'connectionChange')
    .pipe(
      throttleTime(500)
    )
    .subscribe(event => {
      if (event.type == 'none') {
        this.setState({isOffline: true});
      }
      else {
        this.setState({isOffline: false});
      }
      onToggle(event);
    });
  }

  componentWillUnmount() {
    this.subscription.unsubscribe();
    this.subscription = null;
  }

  render() {
    if (!this.props.showBanner) return null;
    if (!this.state.isOffline) return null;
    return (
      <Toast
        visible
        position={50}
        shadow={false}
        animation
        hideOnPress
        backgroundColor={Theme.colors.error}
      >
        No Internet Connection
      </Toast>
    );
  }
}

export default OfflineNotice;
