import axios from '../../utils/axios-interceptor';


export default class BaseApiGateway {
  configEndpoint = null;

  headers = {
    Accept: 'application/json', 'Content-Type': 'application/json',
  };

  get = {
    method: 'GET',
    headers: this.headers,
  };

  post = {
    method: 'POST',
    headers: this.headers,
  };

  put = {
    method: 'PUT',
    headers: this.headers,
  };

  constructor(config) {
    this.configEndpoint = config;
  }

  async load() {
    return axios(BaseApiGateway.generateHeader(this.configEndpoint, null, this.get, false));
  }

  async save(dto) {
    return axios(BaseApiGateway.generateHeader(this.configEndpoint, dto, this.post, true));
  }

  async update(dto) {
    return axios(BaseApiGateway.generateHeader(this.configEndpoint, dto, this.put, true));
  }

  static generateHeader(url, dto, headerArg, hasData) {
    const header = headerArg;
    header.url = url;
    if (hasData) header.data = JSON.stringify(dto);
    return header;
  }
}
