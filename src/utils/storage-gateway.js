import {
    AsyncStorage,
  } from 'react-native';
  
export default class BaseDBGateway {
  static async set(key, value) {
    try {
      await AsyncStorage.setItem(key, value);
    } catch (ex) {
      console.log(ex);
    }
  }

  static async get(key) {
    return await AsyncStorage.getItem(key);
  }
  
}
  