import StorageGateway from './storage-gateway';

let instance = null;
class AuthSingleton {
  constructor() {
    if (instance) return instance;
    instance = this;
    instance.authToken = null;
    instance.User_ID = null;
    instance.UserType = null;
    return instance;
  }

  async loadAuthToken() {
    const authToken = await StorageGateway.get('authToken');
    instance.authToken = authToken;
  }

  async setAuthToken(authToken) {
    await StorageGateway.set('authToken', authToken);
    instance.authToken = authToken;
  }

  getAuthToken() {
    return instance.authToken;
  }

  async resetAuthToken() {
    await StorageGateway.set('authToken', null);
    instance.authToken = null;
  }

  //////
  async loadUserID() {
    const UserID = await StorageGateway.get('UserID');
    instance.User_ID = UserID;
  }

  async setUserID(UserID) {
    await StorageGateway.set('UserID', UserID);
    instance.User_ID = UserID;
    console.log('ID value', instance.User_ID);
  }

  getUserID() {
    return instance.User_ID;
  }

  ////
  async loadUserType() {
    const UserType = await StorageGateway.get('UserType');
    instance.User_Type = UserType;
  }

  async setUserType(UserType) {
    await StorageGateway.set('UserType', UserType);
    instance.User_Type = UserType;
    console.log('Type value', instance.User_Type);
  }

  getUserType() {
    return instance.User_Type;
  }
}

export default AuthSingleton;
