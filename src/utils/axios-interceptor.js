import axios from 'axios';
import AuthSingleton from './auth-singleton';

// Add a request interceptor
axios.interceptors.request.use(
  (configuration) => {
    const axiosConfig = configuration;
    const authSingleton = new AuthSingleton();
    axiosConfig.headers.is_mobile = false;
    axiosConfig.apiKey = 'bcgpYPPjzRg1mvSRk47vr2bgqBPwJVT1W6VPaS0ypE0='
    const authToken = authSingleton.getAuthToken();
    if (authToken) {
      axiosConfig.headers.Authorization = `${authToken}`;
      axiosConfig.headers.is_mobile = true;
    }
    return axiosConfig;
  },
  error => Promise.reject(error),
);

export default axios;
