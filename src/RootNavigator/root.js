import React from 'react';
import {
  createSwitchNavigator,
  createStackNavigator,
  createAppContainer,
  createBottomTabNavigator,
} from 'react-navigation';
import SplashScreenPage from '../pages/SplashScreen';
import Landing from '../pages/Landing';
import Assets from '../assets';

import {View, Image, Platform} from 'react-native';

import Register from '../pages/Register';
import SignIn from '../pages/SignIn';
import Dashboard from '../pages/Dashboard';
import CategoryItem from '../pages/Dashboard/components/categoryItem';
import SubCategoryItem from '../pages/Dashboard/components/subCategoryItem';

import Mobile from '../pages/Setting/components/Mobile';
import Settings from '../pages/Setting/index';

import Search from '../pages/Search';

import OtpVerification from '../pages/OtpVerification';
import ForgotPassword from '../pages/ForgotPassword';
import NewPassword from '../pages/ForgotPassword/components/NewPassword';
import FormsCategoryItem from '../pages/Dashboard/components/formsCategoryItem';
import MainDashBoard from '../pages/MainDashboard/indes';

const DashboardStack = createStackNavigator(
  {
    MainDashBoard: {
      path: '/mainDashBoard',
      screen: MainDashBoard,
      navigationOptions: () => ({
        header: null,
      }),
    },
    Dashboard: {
      path: '/dashboard',
      screen: Dashboard,
      navigationOptions: () => ({
        header: null,
      }),
    },

    CategoryItem: {
      path: '/categoryItem',
      screen: CategoryItem,
      navigationOptions: () => ({
        header: null,
      }),
    },
    FormsCategoryItem: {
      path: '/formsCategoryItem',
      screen: FormsCategoryItem,
      navigationOptions: () => ({
        header: null,
      }),
    },
    SubCategoryItem: {
      path: '/subCategoryItem',
      screen: SubCategoryItem,
      navigationOptions: () => ({
        header: null,
      }),
    },
  },
  {
    initialRouteName: 'MainDashBoard',
  },
);

const SettingStack = createStackNavigator(
  {
    Settings: {
      path: '/settings',
      screen: Settings,
      navigationOptions: () => ({
        header: null,
      }),
    },
    Mobile: {
      path: '/mobile',
      screen: Mobile,
      navigationOptions: () => ({
        header: null,
      }),
    },
  },
  {
    initialRouteName: 'Settings',
  },
);

const SearchStack = createStackNavigator(
  {
    Search: {
      path: '/search',
      screen: Search,
      navigationOptions: () => ({
        header: null,
      }),
    },
  },
  {
    initialRouteName: 'Search',
  },
);

const AuthStack = createStackNavigator(
  {
    Landing: {
      path: '/landing',
      screen: Landing,
      navigationOptions: () => ({
        header: null,
      }),
    },
    SignIn: {
      path: '/signin',
      screen: SignIn,
      navigationOptions: () => ({
        header: null,
      }),
    },
    Register: {
      path: '/register',
      screen: Register,
      navigationOptions: () => ({
        header: null,
      }),
    },
    OtpVerification: {
      path: 'otpVerification',
      screen: OtpVerification,
      navigationOptions: () => ({
        header: null,
      }),
    },
    ForgotPassword: {
      path: 'forgotPassword',
      screen: ForgotPassword,
      navigationOptions: () => ({
        header: null,
      }),
    },
    NewPassword: {
      path: 'newPassword',
      screen: NewPassword,
      navigationOptions: () => ({
        header: null,
      }),
    },
  },
  {
    navigationOptions: () => ({
      headerBackTitle: 'Back',
    }),
    initialRouteName: 'Landing',
  },
);

const TabNavigator = createBottomTabNavigator(
  {
    Dashboard: {
      screen: DashboardStack,
      navigationOptions: () => ({
        tabBarIcon: () => (
          <Image
            source={Assets.HomeIcon}
            style={{
              height: Platform.OS === 'ios' ? 20 : 25,
              width: Platform.OS === 'ios' ? 20 : 25,
              marginTop: 8,
            }}
          />
        ),
        tabBarLabel: 'Home',
      }),
    },
    Profile: {
      screen: SettingStack,
      navigationOptions: () => ({
        tabBarIcon: () => (
          <Image
            source={Assets.ProfileIcon}
            style={{
              height: Platform.OS === 'ios' ? 20 : 24,
              width: Platform.OS === 'ios' ? 22 : 24,
              marginTop: 8,
            }}
          />
        ),
        tabBarLabel: 'Profile',
      }),
    },
  },
  {
    initialRouteName: 'Dashboard',
    tabBarOptions: {
      activeTintColor: '#ffb600',
      inactiveTintColor: 'gray',
      style: {borderTopWidth: 0, elevation: 6},
    },
  },
);

const MainDashboardStack = createStackNavigator(
  {
    Dashboard: {
      screen: TabNavigator,
      navigationOptions: () => ({
        header: null,
      }),
    },
  },
  {
    initialRouteName: 'Dashboard',
  },
);

const RootNavigator = createSwitchNavigator(
  {
    SplashScreen: SplashScreenPage,
    Auth: AuthStack,
    App: MainDashboardStack,
  },
  {
    initialRouteName: 'SplashScreen',
  },
);

export default createAppContainer(RootNavigator);
