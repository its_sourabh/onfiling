import { NavigationActions, DrawerActions } from 'react-navigation';

let _navigator;

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
}

function navigate(routeName, params) {
  _navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params
    })
  );
}

function back(routeName) {
  _navigator.dispatch(NavigationActions.back());
}

function setParams(routeName, params) {
  _navigator.dispatch(
    NavigationActions.setParams({
      params,
      key: routeName
    })
  );
}

function toggleDrawer() {
  _navigator.dispatch(DrawerActions.toggleDrawer());
}

// add other navigation functions that you need and export them
export default {
  navigate,
  back,
  setParams,
  toggleDrawer,
  setTopLevelNavigator
};
