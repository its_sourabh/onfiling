import React from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Platform,Dimensions} from 'react-native';
import Theme from '../styles/theme';
import Spacing from '../styles/spacing';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const isPhoneX = Platform.OS === "ios" && (height >= 812 || width >= 812);
const drawerCommonNavigationOptions = navigation => ({
  headerStyle: {
    backgroundColor: Theme.colors.surface,
    height:isPhoneX ? 100 :Spacing.height7 ,
  },
  headerTintColor: Theme.colors.primary,
  headerTitleStyle: {
    fontWeight: 'bold',
    textAlign:"center",
  },  
});

export default drawerCommonNavigationOptions;
