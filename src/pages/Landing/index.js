import React from 'react';
import {
  View,
  StyleSheet,
  ImageBackground,
  Text,
  TextInput,
  Image,
} from 'react-native';
import {Button} from 'react-native-elements';
import Router from '../../RootNavigator/Router';
import Images from '../../assets';
import typography from '../../styles/typography';
import layout from '../../styles/layout';
import {PagerDotIndicator, IndicatorViewPager} from 'rn-viewpager';

import Assets from '../../assets';
export default class Landing extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
    };
  }
  renderDotIndicator() {
    return (
      <PagerDotIndicator
        selectedDotStyle={{backgroundColor: '#FBB33C'}}
        pageCount={3}
      />
    );
  }
  render() {
    return (
      <View style={[styles.safeArea, {justifyContent: 'space-between'}]}>
        <IndicatorViewPager
          style={{flex: 1, paddingTop: 70}}
          indicator={this.renderDotIndicator()}>
          <Image
            style={{height: '100%', width: '100%'}}
            source={Images.introImage1}
          />

          <Image
            style={{height: '100%', width: '100%'}}
            source={Images.introImage2}
          />
          <Image
            style={{height: '100%', width: '100%'}}
            source={Images.introImage3}
          />
        </IndicatorViewPager>

        <View style={{flexDirection: 'column', position: 'absolute'}}>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 15,
              marginLeft: 10,
              marginRight: 10,
            }}>
            <View
              style={{
                height: '100%',
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                style={{
                  marginTop: 4,
                  height: 40,
                  width: 180,
                  resizeMode: 'contain',
                }}
                source={Images.logo_d}
              />
            </View>
          </View>
        </View>

        <View
          style={{
            flexDirection: 'column',
            width: '100%',
          }}>
          <View
            style={[
              styles.contentCenter,
              {margin: 10, backgroundColor: 'black'},
            ]}>
            <Button
              titleStyle={[typography.button, layout.contentCenter]}
              title="SIGN IN AND EXPLORE"
              onPress={() => Router.navigate('SignIn')}
              buttonStyle={{
                backgroundColor: '#ffb600',
                width: '100%',
                height: '100%',
              }}
              loading={this.state.isLoading}
              containerStyle={[
                layout.contentCenter,
                {
                  height: 55,
                  width: '100%',
                  borderRadius: 4,
                },
              ]}
            />
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: 'white',
  },
  ImageBackgroundStyle: {
    width: '100%',
    height: '100%',
  },
  containerStyle: {
    marginHorizontal: 12,
    marginVertical: 32,
    justifyContent: 'space-between',
  },
  column: {
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'flex-end',
  },
  contentCenter: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  pageColumnCentered: {
    backgroundColor: 'white',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  top: {
    marginVertical: 20,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
});
