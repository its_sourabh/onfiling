import React, {Component} from 'react';
import {
  View,
  TextInput,
  Text,
  SafeAreaView,
  StyleSheet,
  ImageBackground,
  KeyboardAvoidingView,
  AsyncStorage,
} from 'react-native';
import typography from '../../styles/typography';
import Images from '../../assets/index';

import AuthSingleton from '../../utils/auth-singleton';
import axios from 'axios';
import {Button} from 'react-native-elements';
import Router from '../../RootNavigator/Router';
export default class OTPVerification extends Component {
  constructor(props) {
    super(props);
    console.log('props', props);
    this.moveToSecond = this.moveToSecond.bind(this);
    this.moveToThird = this.moveToThird.bind(this);
    this.moveToFourth = this.moveToFourth.bind(this);
    this.moveToNext = this.moveToNext.bind(this);
    this.state = {
      firstCode: '',
      secondCode: '',
      thirdCode: '',
      fourthCode: '',
      code: '',
      isLoading: false,
    };
    // this.setSessionData = this.setSessionData.bind(this);
    this.SendOtpToServer = this.SendOtpToServer.bind(this);
  }
  // setSessionData() {

  // }
  SendOtpToServer() {
    this.setState({isLoading: true});
    var emailId = this.props.navigation.state.params.emailId;
    var password = this.props.navigation.state.params.password;
    var contactNo = this.props.navigation.state.params.contactNo;
    var otp = this.state.code;

    console.log('emailId', emailId);
    console.log('password', password);
    console.log('contactNo', contactNo);
    console.log('otp', otp);

    axios
      .post('http://13.126.234.28/api/auth/user/otp/verification', {
        emailId: emailId,
        password: password,
        contactNo: contactNo,
        otp: otp,
        supplierId: '1',
        requestForm: 'app',
      })
      .then(function(response) {
        console.log(response.data.accessToken);
        // this.setSessionData();
        let access_Token = response.data.accessToken;
        const authSingleton = new AuthSingleton();
        authSingleton.setAuthToken(access_Token);
        Router.navigate('Dashboard');
      })
      .catch(function(error) {
        console.log(error);
        this.setState({isLoading: false});
        Router.navigate('OtpVerification');
        Alert.alert('Dogma', 'OTP is not valid');
      });
  }
  moveToSecond(text) {
    this.setState({firstCode: text});
    this.refs.input2.focus();
  }
  moveToThird(text) {
    this.setState({secondCode: text});
    this.refs.input3.focus();
  }
  moveToFourth(text) {
    this.setState({thirdCode: text});
    this.refs.input4.focus();
  }
  moveToNext(text) {
    this.setState({fourthCode: text});
    let one = this.state.firstCode.toString();
    let two = this.state.secondCode.toString();
    let three = this.state.thirdCode.toString();
    let four = text.toString();
    let otp = +''.concat(one, two, three, four);
    this.setState({code: otp});
    // this.props.value=this.props.data.code;
  }

  render() {
    return (
      <ImageBackground
        source={Images.registerBackground}
        style={{width: '100%', height: '100%'}}>
        <SafeAreaView style={styles.column}>
          <View style={{marginLeft: 36, marginRight: 36}}>
            <Text
              style={[
                styles.Space,
                {
                  color: '#ffb600',
                  fontSize: 28,
                  marginTop: 80,
                  fontWeight: 'bold',
                },
              ]}>
              Verification Code
            </Text>
            <Text style={[styles.body, styles.textstyle]}>
              {' '}
              Please enter the 4 digit verification Code{'\n'} sent on entered
              mobile no.
            </Text>
            <View style={[styles.top, styles.contentCenter]}>
              <Text style={[styles.caption, styles.contentCenter]}>
                Enter the code
              </Text>
              <View
                style={[styles.verticalMargin, styles.row, styles.selfCenter]}>
                <View style={styles.verticalMargin} />
                <TextInput
                  style={styles.input}
                  underlineColorAndroid="transparent"
                  onChangeText={text => {
                    this.moveToSecond(text);
                  }}
                  keyboardType="numeric"
                  maxLength={1}
                  textAlign="center"
                  autoFocus
                />
                <TextInput
                  style={styles.input}
                  ref="input2"
                  keyboardType="numeric"
                  maxLength={1}
                  onChangeText={text => {
                    this.moveToThird(text);
                  }}
                  textAlign="center"
                  underlineColorAndroid="transparent"
                />
                <TextInput
                  style={styles.input}
                  ref="input3"
                  keyboardType="numeric"
                  maxLength={1}
                  onChangeText={text => {
                    this.moveToFourth(text);
                  }}
                  underlineColorAndroid="transparent"
                  textAlign="center"
                />
                <TextInput
                  style={styles.input}
                  ref="input4"
                  keyboardType="numeric"
                  onChangeText={text => {
                    this.moveToNext(text);
                  }}
                  maxLength={1}
                  underlineColorAndroid="transparent"
                  textAlign="center"
                />
              </View>
              <View style={[styles.verticalMargin, styles.contentCenter]}>
                <Button
                  title="Verify                        "
                  titleStyle={[typography.button, styles.contentCenter]}
                  //onPress={() => Router.navigate('Dashboard')}
                  onPress={this.SendOtpToServer}
                  buttonStyle={{
                    backgroundColor: '#ffb600',
                    width: '100%',
                    height: '100%',
                  }}
                  loading={this.state.isLoading}
                  containerStyle={{
                    height: 50,
                    borderColor: '#ffb600',
                    width: '100%',
                    borderRadius: 4,
                    borderWidth: 1,
                    margin: 10,
                  }}
                />
              </View>
              <View
                style={[styles.selfCenter, styles.row, styles.verticalMargin]}>
                <Text style={styles.caption}>Didn't get the code ? </Text>
                <Text style={[styles.caption, styles.TextPrimary]}>
                  Tap to Resend
                </Text>
              </View>
            </View>
          </View>
        </SafeAreaView>
      </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center', // in column axis center
    justifyContent: 'flex-start',
  },

  // Any other view if wrapped inside row - will itself be a column
  column: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  contentCenter: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  verticalMargin: {
    marginTop: 10,
    marginBottom: 10,
  },
  selfCenter: {
    alignSelf: 'center',
    textAlign: 'center',
  },
  headline: {
    fontSize: 24,
    lineHeight: 32,
    color: 'black',
  },
  body: {
    fontSize: 12,
    lineHeight: 20,
    color: 'black',
  },
  caption: {
    fontSize: 12,
    lineHeight: 16,
    color: 'grey',
  },

  input: {
    marginLeft: 8,
    width: 56,
    height: 55,
    borderRadius: 4,
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: '#ffb600',
  },
  textstyle: {
    color: 'grey',
    marginTop: 8,
  },
  top: {
    marginTop: 38,
  },
  Space: {
    color: 'grey',
    marginTop: 32,
  },
  TextPrimary: {
    color: '#ffb600',
  },
  ButtonWrapper: {
    width: '100%',
    height: 40,
    backgroundColor: '#ffb600',
  },
});
