import AuthSingleton from '../../utils/auth-singleton';
import axios from 'axios';
import EventEmitter from '../../utils/event-emitter';
var qs = require('querystringify');
import {Alert} from 'react-native';
const EVENT_TYPES = {
  SIGNIN_START: 'SIGNIN_START',
  SIGNIN_SUCCESS: 'SIGNIN_SUCCESS',
  SIGNIN_FAILURE: 'SIGNIN_FAILURE',
};
class SignInUsecase {
  eventEmitter = null;
  constructor() {
    this.eventEmitter = new EventEmitter();
  }

  toString() {
    return 'SignInUsecase';
  }

  getObservable() {
    return this.eventEmitter.getObservable();
  }

  execute = async requestObject => {
    this.eventEmitter.emit({
      type: EVENT_TYPES.SIGNIN_START,
    });
    let res = {
      username: requestObject.email,
      password: requestObject.password,
      grant_type: 'password',
    };
    let data_res = qs.stringify(res);
    // requestObject.grant_type="password";
    try {
      const response = await axios.post(
        'http://onfiling1-env.eug9cevh2s.ap-south-1.elasticbeanstalk.com/onfiling/oauth/token',
        data_res,
        {
          headers: {
            Authorization: 'Basic VVNFUl9DTElFTlRfQVBQOnBhc3N3b3Jk',
            'Content-type': 'application/x-www-form-urlencoded;charset=utf-8',
          },
          withCredentials: true,
        },
      );
      console.log('response', response, res);
      console.log('response.data.accessToken', response.data.access_token);
      console.log('response.data.userId', response.data.userId);
      let accessToken = response.data.access_token;
      let user_id = response.data.userId;
      let user_type = response.data.userType;
      const authSingleton = new AuthSingleton();
      await authSingleton.setAuthToken(accessToken);
      await authSingleton.setUserID(user_id);
      await authSingleton.setUserType(user_type);
      this.eventEmitter.emit({
        type: EVENT_TYPES.SIGNIN_SUCCESS,
        data: response,
      });
    } catch (e) {
      console.log('e', e.response);
      this.eventEmitter.emit({
        type: EVENT_TYPES.SIGNIN_FAILURE,
        data: 'Wrong Username Password',
      });
      return;
    }
  };
}
export default SignInUsecase;
