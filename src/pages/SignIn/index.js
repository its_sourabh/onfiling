import React, {PureComponent} from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  Alert,
  Text,
  ImageBackground,
  SafeAreaView,
} from 'react-native';
import {Toast} from 'native-base';
import {Button} from 'react-native-elements';
import SignInUsecase from './usecase';
import Images from '../../assets';
import Router from '../../RootNavigator/Router';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import typography from '../../styles/typography';
import layout from '../../styles/layout';
export default class LoginForm extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      isLoading: false,
    };
    this.signInUsecase = new SignInUsecase();
    this.listenSignInUseCase();
  }

  listenSignInUseCase() {
    this.signInUsecase.getObservable().subscribe(event => {
      switch (event.type) {
        case 'SIGNIN_START':
          this.setState({isLoading: true});
          break;
        case 'SIGNIN_SUCCESS':
          console.log('response before success');
          this.setState({isLoading: false});
          Router.navigate('Dashboard', {openTab: 0});
          break;
        case 'SIGNIN_FAILURE':
          this.setState({
            isLoading: false,
          });
          Toast.show({
            text: event.data,
            buttonText: 'Okay',
            duration: 2000,
            type: 'warning',
          });
          break;
        default:
          console.log('Sorry, we are not handling this');
      }
    });
  }
  render() {
    return (
      <SafeAreaView>
        <ImageBackground
          source={Images.registerBackground}
          style={{width: '100%', height: '100%', marginTop: -1}}>
          <View style={[styles.safeArea, styles.pageColumnCentered]}>
            <Text
              style={[
                {
                  color: 'black',
                  fontSize: 28,
                  justifyContent: 'center',
                  alignSelf: 'center',
                  fontWeight: 'bold',
                  marginTop: 150,
                },
              ]}>
              Signin to Continue
            </Text>
            <View style={{marginTop: 32, marginBottom: 1}} />

            <View style={[styles.row, styles.TextInputContainer]}>
              <View style={[{width: '100%'}]}>
                <TextInput
                  style={[styles.inputFieldWrapper, styles.TextInputStyle]}
                  ref={component => (this._textInput = component)}
                  placeholder="Email"
                  onChangeText={text => this.setState({email: text})}
                  autoCorrect={false}
                  value={this.state.email}
                  clearButtonMode="while-editing"
                  enablesReturnKeyAutomatically
                  keyboardType="email-address"
                  textContentType="username"
                  autoCapitalize="none"
                  returnKeyType="next"
                />
              </View>
            </View>
            <View style={[styles.row, styles.TextInputContainer]}>
              <View style={[{width: '100%'}]}>
                <TextInput
                  style={[styles.inputFieldWrapper, styles.TextInputStyle]}
                  placeholder="Password"
                  onChangeText={text => this.setState({password: text})}
                  autoCorrect={false}
                  value={this.state.password}
                  enablesReturnKeyAutomatically
                  secureTextEntry
                  textContentType="password"
                  autoCapitalize="none"
                  returnKeyType="next"
                />
              </View>
            </View>
            <View style={styles.top} />

            <View style={[styles.contentCenter]}>
              <Button
                titleStyle={[typography.button, layout.contentCenter]}
                title=" Login "
                onPress={() => this.signInUsecase.execute(this.state)}
                buttonStyle={{
                  backgroundColor: '#ffb600',
                  width: '100%',
                  height: '100%',
                  borderRadius: 100,
                }}
                loading={this.state.isLoading}
                containerStyle={[
                  layout.contentCenter,
                  {
                    height: 50,
                    width: '100%',
                    borderColor: '#ffb600',
                    borderRadius: 100,
                    borderWidth: 1,
                    margin: 10,
                  },
                ]}
              />
            </View>
            <Text
              style={[styles.contentCenter]}
              onPress={() => Router.navigate('ForgotPassword')}>
              FORGOT PASSWORD ?
            </Text>
            {/* <View
              style={[styles.row, styles.contentCenter, styles.verticalMargin]}>
              <Text>Don't have account? </Text>
              <Text
                style={{
                  color: '#ffb600',
                  fontSize: 15,
                  textDecorationLine: 'underline',
                }}
                onPress={() => Router.navigate('Register')}>
                Sign up
              </Text>
            </View> */}
          </View>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  contentCenter: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  selfCenter: {
    justifyContent: 'center',
    alignSelf: 'center',
    textAlign: 'center',
  },
  safeArea: {
    flex: 1,
    backgroundColor: 'white',
  },
  pageColumnCentered: {
    marginHorizontal: 34,
    backgroundColor: 'transparent',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  top: {
    marginHorizontal: 40,
  },
  TextInputContainer: {
    marginTop: 8,
    borderRadius: 0,
    width: '100%',
    height: 48,
  },
  TextInputStyle: {
    borderRadius: 4,
    width: '100%',
    paddingLeft: 18,
    height: 50,
    borderColor: '#ffb600',
    borderBottomWidth: 0.5,
  },
  inputFieldWrapper: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: 0,
    marginBottom: 14,
  },
  column: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  verticalMargin: {
    marginTop: 36,
    marginBottom: 12,
  },
});
