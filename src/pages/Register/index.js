import React from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  Alert,
  Text,
  ImageBackground,
} from 'react-native';
import {Toast} from 'native-base';
import {Button} from 'react-native-elements';
import Router from '../../RootNavigator/Router';
import typography from '../../styles/typography';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import RegisterUsecase from './usecase';
import bg2 from '../../assets/index';
export default class Landing extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      firstName: '',
      phoneNumber: '',
      password: '',
      isLoading: false,
    };
    this.registerUsecase = new RegisterUsecase();
    this.listenRegisterUseCase();
  }

  listenRegisterUseCase() {
    var email = this.state.email;
    var password = this.state.password;
    var contactNo = this.state.phoneNumber;
    this.registerUsecase.getObservable().subscribe(event => {
      switch (event.type) {
        case 'REGISTER_START':
          this.setState({isLoading: true});
          break;
        case 'REGISTER_SUCCESS':
          this.setState({isLoading: false});
          let response = event.data;
          if (response.data.message == 'success') {
            Router.navigate('OtpVerification', {
              emailId: email,
              contactNo: contactNo,
              password: password,
            });
          }
          if (response.data.message != 'success') {
            Router.navigate('Register');
            Alert.alert("Protein O' clock", 'Email/Phone already registerd');
          }
          break;
        case 'REGISTER_FAILURE':
          this.setState({
            isLoading: false,
          });
          Toast.show({
            text: event.data,
            buttonText: 'Okay',
            duration: 2000,
            type: 'warning',
          });
          // Alert.alert("Protein O' clock", event.data);
          break;
        default:
          console.log('Sorry, we are not handling this');
      }
    });
  }

  render() {
    return (
      <ImageBackground
        source={bg2.registerBackground}
        style={{width: '100%', height: '100%', marginTop: -1}}>
        <View style={[styles.safeArea, styles.pageColumnCentered]}>
          <Text
            style={[
              {
                color: 'black',
                fontSize: 28,
                justifyContent: 'center',
                alignSelf: 'center',
                fontWeight: 'bold',
                marginTop: 150,
              },
            ]}>
            Signup to Continue
          </Text>
          <View style={{marginTop: 32, marginBottom: 1}} />

          <View style={[styles.row, styles.TextInputContainer]}>
            <View style={[{width: '100%'}]}>
              <TextInput
                style={[styles.inputFieldWrapper, styles.TextInputStyle]}
                placeholder="Name"
                autoCorrect={false}
                underlineColorAndroid="transparent"
                onChangeText={text => this.setState({firstName: text})}
                value={this.state.firstName}
                clearButtonMode="while-editing"
                enablesReturnKeyAutomatically
                returnKeyType="next"
              />
            </View>
          </View>

          <View style={[styles.row, styles.TextInputContainer]}>
            <View style={[{width: '100%'}]}>
              <TextInput
                style={[styles.inputFieldWrapper, styles.TextInputStyle]}
                placeholder="Mobile number"
                autoCorrect={false}
                underlineColorAndroid="transparent"
                onChangeText={text => this.setState({phoneNumber: text})}
                value={this.state.phoneNumber}
                clearButtonMode="while-editing"
                enablesReturnKeyAutomatically
                keyboardType="numeric"
                returnKeyType="next"
              />
            </View>
          </View>

          <View style={[styles.row, styles.TextInputContainer]}>
            <View style={[{width: '100%'}]}>
              <TextInput
                style={[styles.inputFieldWrapper, styles.TextInputStyle]}
                ref={component => (this._textInput = component)}
                placeholder="name@example.com"
                onChangeText={text => this.setState({email: text})}
                autoCorrect={false}
                value={this.state.email}
                clearButtonMode="while-editing"
                enablesReturnKeyAutomatically
                keyboardType="email-address"
                textContentType="username"
                autoCapitalize="none"
                returnKeyType="next"
              />
            </View>
          </View>
          <View style={[styles.row, styles.TextInputContainer]}>
            <View style={[{width: '100%'}]}>
              <TextInput
                style={[styles.inputFieldWrapper, styles.TextInputStyle]}
                placeholder="Password"
                containerStyle={styles.inputFieldWrapper}
                autoCorrect={false}
                onChangeText={text => this.setState({password: text})}
                value={this.state.password}
                enablesReturnKeyAutomatically
                secureTextEntry
                textContentType="password"
                autoCapitalize="none"
                returnKeyType="next"
              />
            </View>
          </View>

          <View style={[styles.contentCenter, styles.verticalMargin]}>
            <Button
              title=" Register "
              titleStyle={[typography.button, styles.contentCenter]}
              onPress={() => this.registerUsecase.execute(this.state)}
              //onPress={() => Router.navigate('OtpVerification')}
              buttonStyle={{
                backgroundColor: '#ffb600',
                width: '100%',
                height: '100%',
                borderRadius: 100,
              }}
              loading={this.state.isLoading}
              containerStyle={{
                height: 50,
                width: '100%',
                borderColor: '#ffb600',
                borderRadius: 100,
                borderWidth: 1,
                margin: 10,
              }}
            />
          </View>
          <View style={[styles.row, styles.contentCenter]}>
            {/* <Icon name="lock" color={'#ffb600'} size={15} /> */}
            <Text>Already have account? </Text>
            <Text
              style={{
                color: '#ffb600',
                fontSize: 15,
                textDecorationLine: 'underline',
              }}
              onPress={() => Router.navigate('SignIn')}>
              Sign In
            </Text>
          </View>
        </View>
      </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({
  contentCenter: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  selfCenter: {
    justifyContent: 'center',
    alignSelf: 'center',
    textAlign: 'center',
  },
  safeArea: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  pageColumnCentered: {
    marginHorizontal: 34,
    backgroundColor: 'transparent',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  top: {
    marginHorizontal: 40,
  },
  TextInputContainer: {
    marginTop: 8,
    borderRadius: 0,
    width: '100%',
    height: 48,
  },
  TextInputStyle: {
    borderRadius: 4,
    width: '100%',
    paddingLeft: 18,
    height: 50,
    borderColor: '#ffb600',
    borderBottomWidth: 0.5,
  },
  inputFieldWrapper: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: 0,
    marginBottom: 14,
  },
  column: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  verticalMargin: {
    marginTop: 36,
    marginBottom: 12,
  },
});
