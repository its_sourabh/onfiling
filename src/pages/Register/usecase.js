import axios from 'axios';
import EventEmitter from '../../utils/event-emitter';
  const EVENT_TYPES = {
    REGISTER_START: 'REGISTER_START',
    REGISTER_SUCCESS: 'REGISTER_SUCCESS',
    REGISTER_FAILURE: 'REGISTER_FAILURE',  
  }
class RegisterUseCase {

  eventEmitter = null;
  constructor() {
    this.eventEmitter = new EventEmitter();
  }

  toString() {
    return 'RegisterUseCase';
  }

  getObservable() {
    return this.eventEmitter.getObservable();
  }

    execute = async (requestObject) => {
        if (!requestObject.firstName) {
            this.eventEmitter.emit({
            type: EVENT_TYPES.REGISTER_FAILURE,
            data: 'Name is required.' // This should an object of Granite Error here
            });
            return;
        }
        if (!requestObject.phoneNumber) {
            this.eventEmitter.emit({
            type: EVENT_TYPES.REGISTER_FAILURE,
            data: 'Phone Number is required.' // This should an object of Granite Error here
            });
            return;
        }  
        if (!requestObject.email) {
        this.eventEmitter.emit({
            type: EVENT_TYPES.REGISTER_FAILURE,
            data: 'An Email is required.' // This should an object of Granite Error here
        });
        return;
        }
        var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(!re.test(requestObject.email))
        {
        this.eventEmitter.emit({
            type: EVENT_TYPES.REGISTER_FAILURE,
            data: 'Email is not in the correct format.'
        });
        return;
        }
        if (!requestObject.password) {
        this.eventEmitter.emit({
            type: EVENT_TYPES.REGISTER_FAILURE,
            data: 'A Password is required.'
        });
        return;
        }   
        try {
          this.eventEmitter.emit({
            type: EVENT_TYPES.REGISTER_START,
          });
          const response = await axios.post('http://13.126.234.28/api/auth/user/create', {
                firstName: requestObject.firstName,
                emailId: requestObject.email,
                password: requestObject.password,
                contactNo: requestObject.phoneNumber,
                customAddress: '1',
                supplierId: '1'
              })
          console.log('response',response);   
          this.eventEmitter.emit({
            type: EVENT_TYPES.REGISTER_SUCCESS,
            data: response
          }); 
         } catch (e) {
          this.eventEmitter.emit({
            type: EVENT_TYPES.REGISTER_FAILURE,
            data: e.response.data.ErrorInfo.Message
          });
          return;
        }  
    }
}
export default RegisterUseCase;
