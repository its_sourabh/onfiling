import EventEmitter from '../../utils/event-emitter';
const EVENT_TYPES = {
  SET_NEW_PASSWORD_START: 'SET_NEW_PASSWORD_START',
  SET_NEW_PASSWORD_SUCCESS: 'SET_NEW_PASSWORD_SUCCESS',
  SET_NEW_PASSWORD_FAILURE: 'SET_NEW_PASSWORD_FAILURE',
};
class SetPasswordUseCase {
  eventEmitter = null;
  constructor() {
    this.eventEmitter = new EventEmitter();
  }

  toString() {
    return 'SetPasswordUseCase';
  }

  getObservable() {
    return this.eventEmitter.getObservable();
  }

  async execute(requestObject) {
    this.eventEmitter.emit({
      type: EVENT_TYPES.SET_NEW_PASSWORD_START,
    });
    if (!requestObject.emailId) {
      this.eventEmitter.emit({
        type: EVENT_TYPES.SET_NEW_PASSWORD_FAILURE,
        data: 'Phone Number is required.', // This should an object of Granite Error here
      });
      return;
    } else {
      this.eventEmitter.emit({
        type: EVENT_TYPES.SET_NEW_PASSWORD_SUCCESS,
      });
    }
  }
}
export default SetPasswordUseCase;
