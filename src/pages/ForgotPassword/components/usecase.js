import EventEmitter from '../../../utils/event-emitter';
  const EVENT_TYPES = {
    VALIDATE_PASSWORD_START: 'VALIDATE_PASSWORD_START',
    VALIDATE_PASSWORD_SUCCESS: 'VALIDATE_PASSWORD_SUCCESS',
    VALIDATE_PASSWORD_FAILURE: 'VALIDATE_PASSWORD_FAILURE',  
  }
class ValidatePasswordUseCase {

  eventEmitter = null;
  constructor() {
    this.eventEmitter = new EventEmitter();
  }

  toString() {
    return 'ValidatePasswordUseCase';
  }

  getObservable() {
    return this.eventEmitter.getObservable();
  }

    async validateNewPassword(requestObject) {        
        if (!requestObject.password) {
            this.eventEmitter.emit({
            type: EVENT_TYPES.VALIDATE_PASSWORD_FAILURE,
            data: 'Password is required.' // This should an object of Granite Error here
            });
            return;
        }        
        else {
            this.eventEmitter.emit({
                type: EVENT_TYPES.VALIDATE_PASSWORD_START
            });
            this.eventEmitter.emit({
                type: EVENT_TYPES.VALIDATE_PASSWORD_SUCCESS
            });
        }    
    }
}
export default ValidatePasswordUseCase;
