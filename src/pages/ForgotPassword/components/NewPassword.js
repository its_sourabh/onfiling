import axios from 'axios';
import React from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  Alert,
  Text,
  ImageBackground,
} from 'react-native';
import {Button} from 'react-native-elements';
import Images from '../../../assets';
import typography from '../../../styles/typography';
import Router from '../../../RootNavigator/Router';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import ValidatePasswordUseCase from './usecase';
export default class Landing extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      OTP: '',
      password: '',
      isLoading: false,
    };
    this.validatePasswordUsecase = new ValidatePasswordUseCase();
    this.listenValidatePasswordUseCase();
    this.ValidateDataFromServer = this.ValidateDataFromServer.bind(this);
  }

  ValidateDataFromServer() {
    var OTP = this.state.OTP;
    var password = this.state.password;
    var emailId = this.props.navigation.state.params.emailId;

    console.log('OTP', OTP);
    console.log('password', password);
    console.log('contactNo', emailId);

    axios
      .get('https://www.onfilings.in/onfiling/userAccount/reset', {
        headers: {
          username: emailId,
          otp: OTP,
          password: password,
        },
      })
      .then(function(response) {
        console.log('sdfsdf', response);
        if (response.data) {
          Router.navigate('SignIn');
        } else {
          Alert.alert('Onfiling', 'Invalid OTP');
        }
      })
      .catch(function(error) {
        console.log(error);
        Alert.alert('Onfiling', 'Invalid Credentials');
      });
  }
  listenValidatePasswordUseCase() {
    this.validatePasswordUsecase.getObservable().subscribe(event => {
      switch (event.type) {
        case 'VALIDATE_PASSWORD_START':
          this.setState({isLoading: true});
          break;
        case 'VALIDATE_PASSWORD_SUCCESS':
          this.setState({isLoading: false});
          this.ValidateDataFromServer();
          break;
        case 'VALIDATE_PASSWORD_FAILURE':
          this.setState({
            isLoading: false,
          });
          Alert.alert('Dogma', event.data);
          break;
        default:
          console.log('Sorry, we are not handling this');
      }
    });
  }

  render() {
    const {usecase} = this.props;
    return (
      <ImageBackground
        source={Images.registerBackground}
        style={{width: '100%', height: '100%'}}>
        <View style={[styles.safeArea, styles.pageColumnCentered]}>
          <Text
            style={[
              styles.contentCenter,
              {
                color: '#ffb600',
                fontSize: 28,
                marginTop: 80,
                marginLeft: -15,
                fontWeight: 'bold',
              },
            ]}>
            Create New Password
          </Text>
          <Text style={[styles.contentCenter, {fontSize: 12}]}>
            Enter OTP sent on your registered emailId. On below{'\n'}feild enter
            new password
          </Text>
          <View style={{marginTop: 32, marginBottom: 16}} />

          <View style={[styles.row, styles.TextInputContainer]}>
            <View style={[{width: '100%'}]}>
              <TextInput
                style={[styles.inputFieldWrapper, styles.TextInputStyle]}
                placeholder="OTP"
                maxLength={6}
                autoCorrect={false}
                underlineColorAndroid="transparent"
                onChangeText={text => this.setState({OTP: text})}
                value={this.state.OTP}
                clearButtonMode="while-editing"
                enablesReturnKeyAutomatically
                keyboardType="numeric"
                returnKeyType="next"
              />
            </View>
          </View>
          <View style={[styles.row, styles.TextInputContainer]}>
            <View style={[{width: '100%'}]}>
              <TextInput
                style={[styles.inputFieldWrapper, styles.TextInputStyle]}
                placeholder="Password"
                containerStyle={styles.inputFieldWrapper}
                autoCorrect={false}
                onChangeText={text => this.setState({password: text})}
                value={this.state.password}
                textContentType="password"
              />
            </View>
          </View>
          <View style={[styles.contentCenter, styles.verticalMargin]}>
            <Button
              titleStyle={[typography.button, styles.contentCenter]}
              title="CREATE NEW PASSWORD"
              onPress={() =>
                this.validatePasswordUsecase.validateNewPassword(this.state)
              }
              buttonStyle={{
                backgroundColor: '#ffb600',
                width: '100%',
                height: '100%',
              }}
              containerStyle={{
                height: 50,
                borderColor: '#ffb600',
                width: '100%',
                borderRadius: 4,
                borderWidth: 1,
                margin: 10,
              }}
              loading={this.state.isLoading}
            />
          </View>
          <View style={[styles.contentCenter]}>
            <Text onPress={() => Router.navigate('SignIn')}>BACK TO LOGIN</Text>
          </View>
        </View>
      </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({
  contentCenter: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  selfCenter: {
    justifyContent: 'center',
    alignSelf: 'center',
    textAlign: 'center',
  },
  safeArea: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  pageColumnCentered: {
    marginHorizontal: 34,
    backgroundColor: 'transparent',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  top: {
    marginHorizontal: 40,
  },
  TextInputContainer: {
    marginTop: 8,
    borderRadius: 0,
    width: '100%',
    height: 55,
  },
  TextInputStyle: {
    borderRadius: 4,
    width: '100%',
    paddingLeft: 18,
    height: 50,
    borderColor: '#ffb600',
    borderWidth: 1,
  },
  inputFieldWrapper: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: 0,
    marginBottom: 14,
  },
  column: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  verticalMargin: {
    marginTop: 36,
    marginBottom: 12,
  },
});
