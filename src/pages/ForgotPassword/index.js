import React, {PureComponent} from 'react';
import axios from 'axios';
import {
  View,
  StyleSheet,
  TextInput,
  Alert,
  Text,
  ImageBackground,
} from 'react-native';
import Router from '../../RootNavigator/Router';
import {Button} from 'react-native-elements';
import typography from '../../styles/typography';
import SetPasswordUsecase from './usecase';
import Images from '../../assets';
export default class LoginForm extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      emailId: '',
      isLoading: false,
    };
    this.setPasswordUsecase = new SetPasswordUsecase();
    this.listenSetPasswordUseCase();
    this.ValidateMobileNoToServer = this.ValidateMobileNoToServer.bind(this);
  }
  ValidateMobileNoToServer() {
    var emailId = this.state.emailId;
    axios
      .get('https://www.onfilings.in/onfiling/userAccount/resetPassword', {
        headers: {
          username: emailId,
        },
      })
      .then(function(response) {
        console.log(response);
        if (response.data) {
          Router.navigate('NewPassword', {
            emailId: emailId,
          });
        } else {
          Alert.alert('Onfiling', 'EMAIL NOT FOUND');
        }
      })
      .catch(function(error) {
        console.log(error);
      });
  }
  listenSetPasswordUseCase() {
    this.setPasswordUsecase.getObservable().subscribe(event => {
      switch (event.type) {
        case 'SET_NEW_PASSWORD_START':
          this.setState({isLoading: true});
          break;
        case 'SET_NEW_PASSWORD_SUCCESS':
          this.setState({isLoading: false});
          this.ValidateMobileNoToServer();
          break;
        case 'SET_NEW_PASSWORD_FAILURE':
          this.setState({
            isLoading: false,
          });
          Alert.alert('Dogma', event.data);
          break;
        default:
          console.log('Sorry, we are not handling this');
      }
    });
  }
  render() {
    return (
      <ImageBackground
        source={Images.registerBackground}
        style={{width: '100%', height: '100%'}}>
        <View
          style={[
            styles.safeArea,
            styles.pageColumnCentered,
            {marginHorizontal: 34},
          ]}>
          <Text
            style={[
              {
                color: '#ffb600',
                fontSize: 28,
                marginTop: -40,
                fontWeight: 'bold',
              },
            ]}>
            Forgot Password
          </Text>
          <Text style={[{fontSize: 12}]}>
            WE JUST NEED YOUR EMAIL TO VERIFY YOUR ACCOUNT
          </Text>
          <View style={[{}]} />
          <View style={[styles.TextInputContainer, styles.verticalMargin]}>
            <TextInput
              style={[styles.inputFieldWrapper, styles.TextInputStyle]}
              placeholder="Enter E-mail"
              autoCorrect={false}
              onChangeText={text => this.setState({emailId: text})}
              value={this.state.emailId}
              clearButtonMode="while-editing"
              enablesReturnKeyAutomatically
              keyboardType="email-address"
              returnKeyType="next"
            />
          </View>

          <View style={[styles.contentCenter]}>
            <Button
              titleStyle={[typography.button, styles.contentCenter]}
              title="SUBMIT"
              buttonStyle={{
                backgroundColor: '#ffb600',
                width: '100%',
                height: '100%',
              }}
              onPress={() => {
                this.setState({isLoading: true});
                this.setPasswordUsecase.execute(this.state);
              }}
              containerStyle={{
                height: 50,
                borderColor: '#ffb600',
                width: '100%',
                borderRadius: 4,
                borderWidth: 1,
                margin: 10,
              }}
              loading={this.state.isLoading}
            />
          </View>
          <Text
            onPress={() => Router.navigate('SignIn')}
            style={[styles.contentCenter, styles.top]}>
            BACK TO LOGIN PAGE
          </Text>
        </View>
      </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({
  contentCenter: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  selfCenter: {
    justifyContent: 'center',
    alignSelf: 'center',
    textAlign: 'center',
  },
  safeArea: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  pageColumnCentered: {
    marginHorizontal: 16,
    backgroundColor: 'transparent',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  top: {
    marginHorizontal: 40,
  },
  TextInputContainer: {
    marginTop: 8,
    borderRadius: 0,
    width: '100%',
    height: 50,
  },
  TextInputStyle: {
    borderRadius: 4,
    width: '100%',
    paddingLeft: 18,
    height: 50,
    borderColor: '#ffb600',
    borderWidth: 1,
  },
  inputFieldWrapper: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: 0,
    marginBottom: 104,
  },
  column: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  verticalMargin: {
    marginTop: 36,
    marginBottom: 12,
  },
});
