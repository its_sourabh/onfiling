import React, {PureComponent} from 'react';
import {
  View,
  StyleSheet,
  Text,
  SafeAreaView,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';
import {SliderBox} from 'react-native-image-slider-box';
import Assets from '../../assets';
import theme from '../../styles/theme';
import Router from '../../RootNavigator/Router';

export default class MainDashboard extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      banner: [Assets.b1, Assets.b2, Assets.b3],
    };
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <Image
          style={{alignSelf: 'center', marginTop: 20}}
          source={Assets.logo_d}
        />
        <SliderBox
          images={this.state.banner}
          parentWidth={Dimensions.get('screen').width}
          sliderBoxHeight={200}
          dotColor={'transparent'}
          inactiveDotColor="transparent"
          autoplay
          circleLoop
          resizeMethod={'resize'}
          resizeMode={'contain'}
          paginationBoxStyle={{
            bottom: 0,
            padding: 0,
            alignItems: 'center',
            alignSelf: 'center',
            justifyContent: 'center',
          }}
          ImageComponentStyle={{
            borderRadius: 8,
            width: '95%',
            marginTop: 5,
          }}
        />
        <View
          style={{
            width: '100%',
            height: '75%',
            padding: '2%',
          }}>
          <Text
            style={{
              fontSize: 18,
              fontWeight: 'bold',
            }}>
            {/* Fast Open */}
          </Text>
          <View style={{width: '100%', flexDirection: 'row'}}>
            <TouchableOpacity
              onPress={() => Router.navigate('Dashboard', {openTab: 0})}
              style={{
                flex: 1,
                alignItems: 'center',
                margin: 2,
                borderWidth: 1,
                paddingVertical: 8,
                borderColor: '#e1e1e1',
              }}>
              <Image
                style={{height: 60, width: 60}}
                source={Assets.homeUpload}
              />
              <Text
                style={{
                  fontWeight: '700',
                  width: '96%',
                  textAlignVertical: 'center',
                  textAlign: 'center',
                  marginTop: 4,
                  color: theme.colors.primary,
                }}>
                Documents
              </Text>
              <Text
                style={{
                  width: '96%',
                  alignSelf: 'center',
                  textAlign: 'center',
                  textAlignVertical: 'center',
                  color: '#a1a1a1',
                }}>
                Fast and easy document upload .
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => Router.navigate('Dashboard', {openTab: 2})}
              style={{
                flex: 1,
                alignItems: 'center',
                margin: 2,
                borderWidth: 1,
                paddingVertical: 8,
                borderColor: '#e1e1e1',
              }}>
              <Image
                style={{height: 60, width: 60}}
                source={Assets.homeCalender}
              />
              <Text
                style={{
                  fontWeight: '700',
                  width: '96%',
                  textAlignVertical: 'center',
                  textAlign: 'center',
                  marginTop: 4,
                  color: theme.colors.primary,
                }}>
                Calender
              </Text>
              <Text
                style={{
                  width: '96%',
                  alignSelf: 'center',
                  textAlign: 'center',
                  textAlignVertical: 'center',
                  color: '#a1a1a1',
                }}>
                Get Updates for all your upcoming compliance.
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => Router.navigate('Dashboard', {openTab: 3})}
              style={{
                flex: 1,
                alignItems: 'center',
                margin: 2,
                borderWidth: 1,
                paddingVertical: 8,
                borderColor: '#e1e1e1',
              }}>
              <Image style={{height: 60, width: 60}} source={Assets.homeChat} />
              <Text
                style={{
                  fontWeight: '700',
                  width: '96%',
                  textAlignVertical: 'center',
                  textAlign: 'center',
                  marginTop: 4,
                  color: theme.colors.primary,
                }}>
                Support
              </Text>
              <Text
                style={{
                  width: '96%',
                  alignSelf: 'center',
                  textAlign: 'center',
                  textAlignVertical: 'center',
                  color: '#a1a1a1',
                }}>
                Chat for queries resolution.
              </Text>
            </TouchableOpacity>
          </View>

          <View style={{width: '100%', flexDirection: 'row', marginTop: 8}}>
            <TouchableOpacity
              onPress={() => Router.navigate('Settings')}
              style={{
                flex: 1,
                alignItems: 'center',
                margin: 2,
                borderWidth: 1,
                paddingVertical: 8,
                borderColor: '#e1e1e1',
              }}>
              <Image
                style={{height: 60, width: 60}}
                source={Assets.homeProfile}
              />
              <Text
                style={{
                  fontWeight: '700',
                  width: '96%',
                  textAlignVertical: 'center',
                  textAlign: 'center',
                  marginTop: 10,
                  color: theme.colors.primary,
                }}>
                Profile
              </Text>
              <Text
                style={{
                  width: '96%',
                  alignSelf: 'center',
                  textAlign: 'center',
                  textAlignVertical: 'center',
                  color: '#a1a1a1',
                }}>
                Update your profile in few clicks
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => Router.navigate('Dashboard', {openTab: 1})}
              style={{
                flex: 1,
                alignItems: 'center',
                margin: 2,
                borderWidth: 1,
                paddingVertical: 8,
                borderColor: '#e1e1e1',
              }}>
              <Image
                style={{height: 60, width: 60}}
                source={Assets.homePackages}
              />
              <Text
                style={{
                  fontWeight: '700',
                  width: '96%',
                  textAlignVertical: 'center',
                  textAlign: 'center',
                  marginTop: 4,
                  color: theme.colors.primary,
                }}>
                Packages
              </Text>
              <Text
                style={{
                  width: '96%',
                  alignSelf: 'center',
                  textAlign: 'center',
                  textAlignVertical: 'center',
                  color: '#a1a1a1',
                }}>
                Explore Services
              </Text>
            </TouchableOpacity>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}
            />
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
