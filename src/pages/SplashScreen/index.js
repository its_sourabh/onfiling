import React from 'react';
import {
  ActivityIndicator,
  StatusBar,
  StyleSheet,
  View,
  SafeAreaView,
} from 'react-native';
import AuthSingleton from '../../utils/auth-singleton';
import Router from '../../RootNavigator/Router';
export default class SplashScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      isLoading: false,
      errorMessage: '',
    };
    this._bootstrapAsync();
  }
  async _bootstrapAsync() {
    const authSingleton = new AuthSingleton();
    await authSingleton.loadAuthToken();
    await authSingleton.loadUserID();
    const authToken = authSingleton.getAuthToken();
    console.log('auth token', authToken);
    Router.navigate(authToken ? 'MainDashBoard' : 'Auth');
  }
  render() {
    return (
      <SafeAreaView style={styles.safeArea}>
        <View style={styles.pageColumnCentered}>
          <ActivityIndicator />
          <StatusBar barStyle="default" />
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: 'white',
  },
  pageColumnCentered: {
    marginHorizontal: 8,
    backgroundColor: 'white',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
});
