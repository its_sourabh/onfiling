import React, {Component} from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import typography from '../../styles/typography';
import ProfileCard from './components/profileCard';
import {Tabs, TabHeading, Tab, ScrollableTab} from 'native-base';
import {Icon} from 'react-native-elements';
import DocumentTabComponent from './components/TabComponents/documents';
import Axios from 'axios';
import Forms from './components/TabComponents/forms';
import CalendarTabComponent from './components/TabComponents/calender';
import SupportTabComponent from './components/TabComponents/Support';
import ChatView from './components/TabComponents/chatScreenDesign';
import FormCategoryTabComponent from './components/TabComponents/formsCategory';
import AuthSingleton from '../../utils/auth-singleton';

export default class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {dropdownCategory: [], categoryData: []};
  }

  render() {
    return (
      <View style={styles.container}>
        <ProfileCard />
        <View style={{height: '75%', width: '100%'}}>
          <Tabs
            activeTextStyle={{color: 'red'}}
            initialPage={
              this.props.navigation.state.params.openTab
                ? this.props.navigation.state.params.openTab
                : 0
            }
            tabBarUnderlineStyle={{backgroundColor: '#ffb600'}}
            renderTabBar={() => <ScrollableTab />}>
            <Tab
              heading={
                <TabHeading style={{backgroundColor: 'white'}}>
                  <Icon iconStyle={{color: '#ffb600'}} name="folder" />
                  <Text> Documents</Text>
                </TabHeading>
              }>
              <DocumentTabComponent />
            </Tab>
            <Tab
              heading={
                <TabHeading style={{backgroundColor: 'white'}}>
                  <Icon iconStyle={{color: '#ffb600'}} name="assignment" />
                  <Text>Packages</Text>
                </TabHeading>
              }>
              <FormCategoryTabComponent />
            </Tab>
            <Tab
              heading={
                <TabHeading style={{backgroundColor: 'white'}}>
                  <Icon iconStyle={{color: '#ffb600'}} name="today" />
                  <Text> Calender</Text>
                </TabHeading>
              }>
              <CalendarTabComponent />
            </Tab>
            <Tab
              heading={
                <TabHeading style={{backgroundColor: 'white'}}>
                  <Icon iconStyle={{color: '#ffb600'}} name="chat" />
                  <Text> Support</Text>
                </TabHeading>
              }>
              {/* <SupportTabComponent /> */}
              <View style={{flex: 1, backgroundColor: 'red', marginBottom: 10}}>
                <ChatView />
              </View>
            </Tab>
          </Tabs>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    width: '100%',
    height: '100%',
  },
});
