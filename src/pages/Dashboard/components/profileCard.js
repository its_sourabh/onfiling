import React, {Component} from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import typography from '../../../styles/typography';
import Axios from 'axios';
import AuthSingleton from '../../../utils/auth-singleton';
import Router from '../../../RootNavigator/Router';

export default class ProfileCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ProfileData: undefined,
    };
  }

  GetProfileDataFromServer = async () => {
    const authSingleton = new AuthSingleton();
    authSingleton.loadAuthToken();
    const authToken = authSingleton.getAuthToken();
    const userId = authSingleton.getUserID();
    console.log('authToken', authToken);
    let response = await Axios.get(
      'http://onfiling1-env.eug9cevh2s.ap-south-1.elasticbeanstalk.com/onfiling/ClientRegistration/all',
      {
        headers: {
          Authorization: 'Bearer ' + authToken,
          'Content-Type': 'application/json',
          id: userId,
        },
      },
    );
    console.log('response', response);
    this.setState({
      ProfileData: response.data.object[0],
    });
    console.log('ProfileData in ', this.state.ProfileData);
  };
  componentDidMount() {
    this.GetProfileDataFromServer();
  }
  render() {
    return (
      <View
        style={{
          backgroundColor: 'white',
          height: '20%',
          width: '100%',
          alignItems: 'center',
          marginVertical: '5%',
        }}>
        <View
          noShadow
          style={{
            height: '95%',
            marginTop: 5,
            width: '95%',
            backgroundColor: 'white',
            borderRadius: 5,
            flexDirection: 'row',
          }}>
          <View
            style={{
              height: '100%',
              width: '30%',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Image
              style={{width: 95, height: 95, borderRadius: 5}}
              source={{uri: 'https://i.ibb.co/rpggHW4/dfghj.jpg'}}
            />
          </View>
          <View
            style={{
              height: '100%',
              width: '70%',
              flexDirection: 'column',
              padding: 5,
              justifyContent: 'center',
            }}>
            <Text
              onPress={() => Router.navigate('MainDashBoard')}
              style={[
                typography.title,
                {fontWeight: 'bold', color: '#ffb600'},
              ]}>
              {this.state.ProfileData ? this.state.ProfileData.clientName : ''}
            </Text>
            <Text style={[typography.subheading, {fontWeight: '700'}]}>
              {this.state.ProfileData ? this.state.ProfileData.companyName : ''}
            </Text>
            <Text style={[typography.body]}>
              {this.state.ProfileData
                ? this.state.ProfileData.companyAddress
                : ''}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
});
