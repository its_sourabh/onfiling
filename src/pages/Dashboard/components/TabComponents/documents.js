import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  Text,
  Dimensions,
  TouchableOpacity,
  Modal,
  Alert,
  Image,
  TextInput,
} from 'react-native';
import {Card, Thumbnail, Toast} from 'native-base';
import DocumentPicker from 'react-native-document-picker';
import Layout from '../../../../styles/layout';
import Theme from '../../../../styles/theme';
import Spacing from '../../../../styles/spacing';
import Typography from '../../../../styles/typography';
import {Dropdown} from 'react-native-material-dropdown';
import assets from '../../../../assets/index';
import {Icon, Button} from 'react-native-elements';

import Router from '../../../../RootNavigator/Router';
import typography from '../../../../styles/typography';
import layout from '../../../../styles/layout';
import AuthSingleton from '../../../../utils/auth-singleton';
import Axios from 'axios';

export default class DocumentTabComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedFiles: [],
      uploadModalVisible: false,
      dropdownSubCategory: [],
      selectedSubCategoryId: undefined,
      selectedCategoryId: undefined,
      dropdownCategory: [],
      categoryData: [],
      editTextOfIndex: -1,
    };
  }

  componentWillMount() {
    this.getCategoryData();
  }

  getCategoryData() {
    var authSingleton = new AuthSingleton();
    var authToken = authSingleton.getAuthToken();
    var userId = authSingleton.getUserID();
    Axios.get(
      'http://onfiling1-env.eug9cevh2s.ap-south-1.elasticbeanstalk.com/onfiling/Category/all',
      {
        headers: {
          Authorization: 'Bearer ' + authToken,
          'Content-Type': 'application/json',
          clientId: userId,
        },
      },
    )
      .then(response => {
        console.log('Response in Dashboard', response);
        this.makedropDownCategotyList(response.data.object);
      })
      .catch(function(error) {
        console.log('Error in GetDataFromServer', error);
        // handle error
        console.log(error);
      });
  }
  makedropDownCategotyList(response) {
    var ee = [];
    for (var i = 0; i < response.length; i++) {
      var dd = {
        value: response[i].categoryName,
        id: response[i].id,
      };
      ee.push(dd);
    }
    this.setState({categoryData: response, dropdownCategory: ee});
  }

  setUploadModalVisible(visible) {
    this.setState({uploadModalVisible: visible});
  }

  getSubCategoryDataFromCategoryId(categoryId) {
    var authSingleton = new AuthSingleton();
    var authToken = authSingleton.getAuthToken();
    var userId = authSingleton.getUserID();
    var url =
      'http://onfiling1-env.eug9cevh2s.ap-south-1.elasticbeanstalk.com/onfiling/SubCategory/all';

    Axios.get(url, {
      headers: {
        Authorization: 'Bearer ' + authToken,
        'Content-Type': 'application/json',
        categoryId: categoryId,
      },
    })
      .then(response => {
        console.log('response', response);
        this.makedropDownSubCategotyList(response.data.object);
      })
      .catch(error => {
        console.log('error', error);
      });
  }

  makedropDownSubCategotyList(response) {
    var ee = [];
    for (var i = 0; i < response.length; i++) {
      var dd = {
        value: response[i].subCategoryName,
        id: response[i].id,
      };
      ee.push(dd);
    }
    this.setState({dropdownSubCategory: ee});
  }

  // _selectFileComponent(selectedFiles) {
  //   var length = selectedFiles.length;
  //   console.log('length of selected files is ', length);
  //   if (length === 0) {
  //     return (
  //       <TouchableOpacity
  //         style={[
  //           layout.contentCenter,
  //           {
  //             marginVertical: 16,
  //             paddingHorizontal: 10,
  //             paddingVertical: 20,
  //             backgroundColor: '#cedce2',
  //             borderWidth: 0.5,
  //             borderStyle: 'dashed',
  //             borderColor: Theme.colors.primary,
  //           },
  //         ]}
  //         onPress={() => this._uploadDocument()}>
  //         <Text style={[Typography.subheading, {color: Theme.colors.primary}]}>
  //           Select Files Here
  //         </Text>
  //       </TouchableOpacity>
  //     );
  //   } else {
  //     return (
  //       <FlatList
  //         data={this.state.selectedFiles}
  //         style={{width: '100%', maxHeight: 400}}
  //         renderItem={({item}) => (
  //           <View
  //             style={[
  //               Layout.row,
  //               {justifyContent: 'space-between', marginBottom: 5},
  //             ]}>
  //             <Thumbnail square source={{uri: item.uri}} />
  //             <Text style={{width: '65%'}} numberOfLines={1}>
  //               {item.name}
  //             </Text>
  //             <TouchableOpacity
  //               style={{
  //                 height: 30,
  //                 width: 30,
  //                 justifyContent: 'center',
  //                 alignItems: 'center',
  //               }}
  //               onPress={() => this.deleteItem(this.state.selectedFiles, item)}>
  //               <Icon name="delete" size={20} color="black" />
  //             </TouchableOpacity>
  //           </View>
  //         )}
  //         keyExtractor={item => item.id}
  //       />
  //     );
  //   }
  // }

  _uploadDocument = async () => {
    try {
      const results = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.images],
      });
      this.setState({selectedFiles: results});
      console.log(results);
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  };

  deleteItem(selectedItem, item) {
    console.log('insied sadasd Item', selectedItem[0]);
    console.log('insied item Item', item);
    var filesUpload = selectedItem;
    var len = filesUpload.length;
    console.log('lenth ', len);

    for (var i = 0; i < len; i++) {
      console.log('Iteration', i);
      if (filesUpload[i].name == item.name) {
        //console.log("mAtch");
        filesUpload.pop(filesUpload[i]);
        console.log('After pop Selected Item ', filesUpload);
        break;
      } else {
        //console.log("Not mAtch");
        // newArr.push(selectedItem[i])
      }
    }
    this.setState({selectedFiles: filesUpload});
    this.forceUpdate();
  }

  onCheckboxPress(item) {
    Router.navigate('DashBoard', {
      scroll: item,
      openTab: 0,
    });
  }

  async _uploadAllDocumentToServer() {
    if (
      this.state.selectedCategoryId == undefined ||
      this.state.selectedSubCategoryId == undefined
    ) {
      Alert.alert('ONFILING', 'Select Category and Sub-Category First');

      return;
    } else if (this.state.selectedFiles.length < 1) {
      Alert.alert('ONFILING', 'Select Files First');

      return;
    }
    var allDocument = [];
    allDocument = allDocument.concat(this.state.selectedFiles);
    for (var i = allDocument.length; i > 0; i--) {
      console.log('Files selected', allDocument[i - 1], i);
      await this._uploadDocumentToServer(allDocument[i - 1], i - 1);
    }
    this.setState({selectedFiles: []});
    Alert.alert('ONFILING', 'Document Uploaded SuccessFully');
  }

  _uploadDocumentToServer(fileToBeUploaded, indexOfFile) {
    var authSingleton = new AuthSingleton();
    var userId = authSingleton.getUserID();
    var url =
      'http://onfiling1-env.eug9cevh2s.ap-south-1.elasticbeanstalk.com/onfiling/DocumentUpload/document/upload';

    var body = {
      categoryDTO: {id: this.state.selectedCategoryId},
      subCategoryDTO: {id: this.state.selectedSubCategoryId},
      clientRegistrationDTO: {id: userId},
      docName: fileToBeUploaded.name,
      docDescription: fileToBeUploaded.name,
      imageType: fileToBeUploaded.type,
      imageName: fileToBeUploaded.name,
      imageSize: fileToBeUploaded.size,
    };

    // console.log('body in upload section', body, documentUploadDTO);
    var form = new FormData();
    form.append('documentUploadDTO', JSON.stringify(body));
    form.append('file', fileToBeUploaded);

    Axios.post(url, form)
      .then(response => {
        console.log('Response on Upload', response);
      })
      .catch(error => {
        console.log('Error', error);
      });
  }

  editSelecteditem(item, index) {
    this.setState({editTextOfIndex: index});
  }

  render() {
    const deviceWidth = Dimensions.get('window').width;
    return (
      <View style={styles.container}>
        <View
          style={{
            height: '10%',
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Card
            noShadow
            style={{
              height: '95%',
              flex: 2,
              borderRadius: 40,
              padding: 15,
              alignItems: 'center',
              flexDirection: 'row',
              backgroundColor: 'white',
            }}>
            <Icon name="search" />
            <Text style={{color: 'grey'}}>SEARCH FILES</Text>
          </Card>
          <View
            style={{
              flexDirection: 'row',
              flex: 1,
              justifyContent: 'space-around',
              alignItems: 'center',
            }}>
            <Card
              noShadow
              style={{
                height: 40,
                width: 40,
                borderRadius: 55,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: 'white',
              }}>
              <Image
                style={{height: 20, width: 20}}
                source={assets.filterIcon}
              />
            </Card>
            <TouchableOpacity
              onPress={() => this.setUploadModalVisible(true)}
              activeOpacity={0.8}>
              <Card
                noShadow
                style={{
                  height: 40,
                  width: 40,
                  borderRadius: 55,
                  backgroundColor: 'white',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Image
                  style={{height: 20, width: 20}}
                  source={assets.uploadIcon}
                />
              </Card>
            </TouchableOpacity>
          </View>
        </View>
        {/** Document FlatList */}
        <View
          style={{
            height: '90%',
            width: '100%',
            marginTop: '3%',
          }}>
          <FlatList
            data={this.state.categoryData}
            showsVerticalScrollIndicator={false}
            numColumns={2}
            renderItem={item => (
              <TouchableOpacity
                activeOpacity={0.8}
                style={[styles.flatListSyle]}
                onPress={() =>
                  Router.navigate('CategoryItem', {
                    categoryName: item.item.categoryName,
                    categoryId: item.item.id,
                  })
                }>
                <Card
                  noShadow
                  style={{
                    width: deviceWidth / 2 - 12,
                    height: 120,
                    backgroundColor: 'white',
                    borderRadius: 8,
                    justifyContent: 'space-around',
                    padding: 15,
                    margin: 5,
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <Image
                      style={{width: 32, height: 32}}
                      source={assets.folderIcon}
                    />
                    <View>
                      <Text
                        style={{
                          backgroundColor: '#e7e7e7',
                          color: '#989898',
                          padding: 5,
                          fontWeight: 'bold',
                          borderRadius: 12,
                        }}>
                        {item.item.subCategoryDTOs.length}
                      </Text>
                    </View>
                  </View>
                  <View>
                    <Text
                      numberOfLines={1}
                      style={{fontSize: 14, fontWeight: 'bold'}}>
                      {item.item.categoryName}
                    </Text>
                    <Text style={{fontSize: 10, color: 'grey'}}>
                      {(dd = new Date(item.item.updatedOn).toLocaleString())}
                      {'\n' + '\n'}
                      Updated/Created
                    </Text>
                  </View>
                </Card>
              </TouchableOpacity>
            )}
            keyExtractor={item => item.id}
          />
        </View>
        {/** Upload Document Model */}
        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.uploadModalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View
            style={[
              Layout.safeArea,
              {
                justifyContent: 'center',
                backgroundColor: Theme.colors.placeholder,
              },
            ]}>
            <View
              style={[
                Layout.selfCenter,
                {
                  paddingVertical: 14,
                  paddingHorizontal: 12,
                  width: '90%',
                  backgroundColor: 'white',
                  borderRadius: 16,
                },
              ]}>
              <View style={[styles.innerWrapper]}>
                <Text
                  style={[
                    Layout.selfStart,
                    typography.title,
                    {paddingLeft: 12, fontWeight: 'bold'},
                  ]}>
                  UPLOAD
                </Text>
                <View style={{width: '100%', paddingHorizontal: 4}}>
                  <Dropdown
                    onChangeText={(value, index, data) => {
                      this.setState({
                        selectedCategoryId: data[index].id,
                        selectedSubCategoryId: undefined,
                        dropdownSubCategory: [],
                      });
                      this.getSubCategoryDataFromCategoryId(data[index].id);
                    }}
                    label="Select Category"
                    fontSize={16}
                    data={this.state.dropdownCategory}
                  />
                </View>

                <View
                  style={{
                    width: '100%',
                    paddingHorizontal: 4,
                  }}>
                  {this.state.dropdownSubCategory &&
                  this.state.dropdownSubCategory.length == 0 ? (
                    <View />
                  ) : (
                    <Dropdown
                      onChangeText={(value, index, data) => {
                        this.setState({selectedSubCategoryId: data[index].id});
                        console.log(
                          'dadasdasdasd',
                          this.state.selectedCategoryId,
                          this.state.selectedSubCategoryId,
                        );
                      }}
                      label="Select Sub-Category"
                      fontSize={16}
                      data={this.state.dropdownSubCategory}
                    />
                  )}
                </View>

                <View style={{marginBottom: 25}}>
                  {this.state.selectedFiles.length == 0 ? (
                    <TouchableOpacity
                      style={[
                        layout.contentCenter,
                        {
                          marginVertical: 16,
                          paddingHorizontal: 10,
                          paddingVertical: 20,
                          backgroundColor: '#cedce2',
                          borderWidth: 0.5,
                          borderStyle: 'dashed',
                          borderColor: Theme.colors.primary,
                        },
                      ]}
                      onPress={() => this._uploadDocument()}>
                      <Text
                        style={[
                          Typography.subheading,
                          {color: Theme.colors.primary},
                        ]}>
                        Select Files Here
                      </Text>
                    </TouchableOpacity>
                  ) : (
                    <FlatList
                      data={this.state.selectedFiles}
                      style={{width: '100%', maxHeight: 400}}
                      extraData={this.state.editTextOfIndex}
                      renderItem={({item, index}) => (
                        <View
                          style={[
                            Layout.row,
                            {justifyContent: 'space-between', marginBottom: 5},
                          ]}>
                          <Thumbnail square source={{uri: item.uri}} />
                          {this.state.editTextOfIndex == index ? (
                            <TextInput
                              style={{width: '65%'}}
                              value={item.name}
                              onChangeText={text => {
                                item.name = text;
                                var sd = [];
                                sd = sd.concat(this.state.selectedFiles);
                                sd[this.state.editTextOfIndex].name = text;
                                this.setState({selectedFiles: sd});
                              }}
                              placeholder={'Enter Doc Name'}
                            />
                          ) : (
                            <Text
                              onPress={() => this.editSelecteditem(item, index)}
                              style={{width: '65%'}}
                              numberOfLines={1}>
                              {item.name}
                            </Text>
                          )}

                          {this.state.editTextOfIndex == index ? (
                            <TouchableOpacity
                              style={{
                                height: 30,
                                width: 30,
                                justifyContent: 'center',
                                alignItems: 'center',
                                backgroundColor: Theme.colors.primary,
                                borderRadius: 50,
                              }}
                              onPress={() => {
                                console.log(
                                  'save the valie of edited file',
                                  this.state.selectedFiles,
                                );
                                this.setState({editTextOfIndex: -1});
                              }}>
                              <Icon name="check" size={20} color={'white'} />
                            </TouchableOpacity>
                          ) : (
                            <TouchableOpacity
                              style={{
                                height: 30,
                                width: 30,
                                justifyContent: 'center',
                                alignItems: 'center',
                              }}
                              onPress={() =>
                                this.deleteItem(this.state.selectedFiles, item)
                              }>
                              <Icon name="delete" size={20} color="black" />
                            </TouchableOpacity>
                          )}
                        </View>
                      )}
                      keyExtractor={item => item.id}
                    />
                  )}
                </View>
                <View
                  style={[
                    Layout.row,
                    {
                      width: '100%',
                      justifyContent: 'space-evenly',
                    },
                  ]}>
                  <View style={{width: '45%'}}>
                    <Button
                      title="CLOSE"
                      onPress={() => {
                        this.setUploadModalVisible(
                          !this.state.uploadModalVisible,
                        );
                      }}
                      titleStyle={[
                        Typography.subheading,
                        {
                          color: Theme.colors.primary,
                          fontWeight: 'bold',
                          fontSize: 14,
                        },
                      ]}
                      buttonStyle={{backgroundColor: 'transparent'}}
                      containerStyle={[
                        Layout.contentCenter,
                        styles.buttonStyle,
                      ]}
                    />
                  </View>
                  <View style={{width: '45%'}}>
                    <Button
                      onPress={() => this._uploadAllDocumentToServer()}
                      title="UPLOAD"
                      titleStyle={[
                        Typography.subheading,
                        {
                          color: Theme.colors.textLight,
                          fontWeight: 'bold',
                          fontSize: 14,
                        },
                      ]}
                      buttonStyle={{backgroundColor: 'transparent'}}
                      containerStyle={[
                        Layout.contentCenter,
                        styles.buttonStyle,
                        {
                          backgroundColor: Theme.colors.primary,
                        },
                      ]}
                    />
                  </View>
                </View>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%',
    paddingHorizontal: '2%',
    paddingVertical: '4%',
    backgroundColor: '#e1e8f2',
    alignItems: 'center',
  },
  badgeContainerStyle4: {
    position: 'absolute',
  },
  innerWrapper: {
    width: '100%',
    backgroundColor: 'white',
    padding: 10,
  },
  safeArea: {
    flex: 1,
    backgroundColor: 'white',
  },
  column: {
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
  },
  contentCenter: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  selfCenter: {
    alignSelf: 'center',
    textAlign: 'center',
  },
  pageColumnCentered: {
    marginHorizontal: 40,
    backgroundColor: 'white',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  top: {
    marginVertical: 40,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  IconStyle: {
    marginRight: 4,
  },
  ImageBackgroundStyle: {
    height: '100%',
    width: '98%',
    marginLeft: 0,
    borderRadius: 4,
    resizeMode: 'center',
  },
  listWrapper: {
    height: 200,
    marginRight: 2,
    marginLeft: 2,
    borderRadius: 5,
    marginVertical: 4,
  },
  textWrapper: {
    marginLeft: 8,
    marginBottom: 4,
  },
  cardContainer: {
    marginBottom: 4,
    paddingLeft: 4,
    paddingRight: 4,
    backgroundColor: '#F0F0F0',
  },
  pageColumn: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
  },
  buttonStyle: {
    backgroundColor: Theme.colors.surface,
    height: Spacing.height5,
    borderWidth: 1,
    borderColor: Theme.colors.primary,
    width: '100%',
    borderRadius: 5,
  },
});
