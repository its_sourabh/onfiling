/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, StyleSheet, Text, Image, FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {Content, Accordion} from 'native-base';

import assets from '../../../../assets';
import typography from '../../../../styles/typography';

export default class FormsTabComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      data: [
        {
          code: 1,
          id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
          title: 'First Item',
          color: '#fff',
          iconColor: '#6bade3',
          image: 'circle-blue.png',
          data: [
            {
              id: '1',
              title: 'First Item',
              iconColor: '#6bade3',
            },
            {
              id: '2',
              title: 'Second Item',
              iconColor: '#6bade3',
            },
            {
              id: '3',
              title: 'Third Item',

              iconColor: '#6bade3',
            },
            {
              id: '4',
              title: 'First Item',

              iconColor: '#6bade3',
            },
            {
              id: '5',
              title: 'Second Item',
              iconColor: '#6bade3',
            },
            {
              id: '6',
              title: 'Third Item',
              iconColor: '#6bade3',
            },
            {
              id: '7',
              title: 'First Item',
              iconColor: '#6bade3',
            },
            {
              id: '8',
              title: 'Second Item',
              iconColor: '#6bade3',
            },
            {
              id: '9',
              title: 'Third Item',

              iconColor: '#6bade3',
            },
          ],
        },
        {
          code: 2,
          id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
          title: 'Second Item',
          color: '#6bade3',
          iconColor: '#fff',
          image: 'circle-white.png',
          data: [
            {
              id: '11',
              title: 'First Item',
              iconColor: '#fff',
            },
            {
              id: '12',
              title: 'Second Item',
              iconColor: '#fff',
            },
            {
              id: '13',
              title: 'Third Item',

              iconColor: '#fff',
            },
            {
              id: '14',
              title: 'First Item',

              iconColor: '#fff',
            },
            {
              id: '15',
              title: 'Second Item',
              iconColor: '#fff',
            },
            {
              id: '16',
              title: 'Third Item',

              iconColor: '#fff',
            },
            {
              id: '17',
              title: 'First Item',

              iconColor: '#fff',
            },
            {
              id: '18',
              title: 'Second Item',
              iconColor: '#fff',
            },
            {
              id: '19',
              title: 'Second Item',
              iconColor: '#fff',
            },
          ],
        },

        {
          code: 3,
          id: '58694a0f-3da1-471f-bd96-145571e29d72',
          title: 'Third Item',
          color: '#fff',
          iconColor: '#6bade3',
          image: 'circle-blue.png',
          data: [
            {
              id: '31',
              title: 'First Item',

              iconColor: '#6bade3',
            },
            {
              id: '32',
              title: 'Second Item',
              iconColor: '#6bade3',
            },
            {
              id: '33',
              title: 'Third Item',

              iconColor: '#6bade3',
            },
            {
              id: '34',
              title: 'First Item',

              iconColor: '#6bade3',
            },
            {
              id: '35',
              title: 'Second Item',
              iconColor: '#6bade3',
            },
            {
              id: '36',
              title: 'Third Item',
              iconColor: '#6bade3',
            },
            {
              id: '37',
              title: 'First Item',
              iconColor: '#6bade3',
            },
            {
              id: '38',
              title: 'Second Item',
              iconColor: '#6bade3',
            },
            {
              id: '39',
              title: 'Third Item',

              iconColor: '#6bade3',
            },
          ],
        },
      ],
    };
    this._renderContentImage = this._renderContentImage.bind(this);
  }

  _renderContentImage(item) {
    if (item.code % 2 == 0) {
      <Image style={{height: 50, width: 50}} source={assets.circleBlue} />;
    } else {
      <Image style={{height: 50, width: 50}} source={assets.circleWhite} />;
    }
  }

  _renderHeader(item, expanded) {
    return (
      <View
        style={{
          borderTopRightRadius: 55,
          padding: 10,
          paddingBottom: 20,
          paddingRight: 20,
          paddingLeft: 20,
          backgroundColor: item.color,
        }}>
        <View
          style={{
            marginTop: 16,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <Text
            style={[
              typography.subheading,
              {fontWeight: 'bold', color: item.iconColor},
            ]}>
            AWESOME PACKAGE
          </Text>
          {expanded ? (
            <Icon style={{fontSize: 30, color: item.iconColor}} name="minus" />
          ) : (
            <Icon style={{fontSize: 30, color: item.iconColor}} name="plus" />
          )}
        </View>
        <Text
          style={[
            typography.caption,
            {color: item.iconColor, fontSize: 10, marginTop: -6},
          ]}>
          Description Of The Package
        </Text>
      </View>
    );
  }

  _renderContent(item) {
    console.log(item);
    return (
      <FlatList
        showsVerticalScrollIndicator={false}
        data={item.data}
        style={{width: '100%', backgroundColor: item.color, paddingBottom: 16}}
        numColumns={1}
        renderItem={({item}) => (
          <View
            style={{
              paddingLeft: 12,
              marginVertical: 4,
              flexDirection: 'row',
            }}>
            <Icon
              name="chevron-right"
              size={20}
              style={{color: item.iconColor, paddingRight: 8}}
            />
            <Text style={[typography.body, {color: item.iconColor}]}>
              Package name
            </Text>
          </View>
        )}
        keyExtractor={item => item.id}
      />
    );
  }

  render() {
    return (
      <View style={[styles.safeArea, {backgroundColor: '#f1f4f6'}]}>
        <Content style={{backgroundColor: '#f1f4f6', width: '100%'}}>
          <Accordion
            showsVerticalScrollIndicator={false}
            dataArray={this.state.data}
            animation={true}
            expanded={true}
            renderHeader={this._renderHeader}
            renderContent={this._renderContent}
          />
        </Content>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: 'white',
  },
  ImageBackgroundStyle: {
    width: '100%',
    height: '100%',
  },
  containerStyle: {
    marginHorizontal: 12,
    marginVertical: 32,
    justifyContent: 'space-between',
  },
  column: {
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'flex-end',
  },
  contentCenter: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  pageColumnCentered: {
    backgroundColor: 'white',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  top: {
    marginVertical: 20,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
});
