import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import Axios from 'axios';
import ElevatedView from 'react-native-elevated-view';
import {Svg, Ellipse, Path, Polygon, Rect, Circle} from 'react-native-svg';
import SvgComponent from '../svgComponents/index';
import Router from '../../../../RootNavigator/Router';
import {Spinner} from 'native-base';

export default class FormCategoryTabComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categoryData: [],
      isLoading: true,
    };
  }

  componentDidMount() {
    this.getCategoryDataFromServer();
  }

  getCategoryDataFromServer() {
    Axios.get(
      'http://onfiling1-env.eug9cevh2s.ap-south-1.elasticbeanstalk.com/onfiling/Category/all',
      {
        headers: {
          categorytype: 'CMS',
        },
      },
    )
      .then(response => {
        console.log('FormCategoryTabComponent in Dashboard', response);
        this.setState({categoryData: response.data.object, isLoading: false});
      })
      .catch(error => {
        console.log('Error in GetDataFromServer', error);
        // handle error
        this.setState({isLoading: false});
      });
  }

  render() {
    return (
      <View style={(styles.container, {marginBottom: '1%'})}>
        {this.state.isLoading ? (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              height: 100,
              width: '100%',
            }}>
            <Spinner />
          </View>
        ) : (
          <FlatList
            data={this.state.categoryData}
            keyExtractor={item => item.id}
            showsVerticalScrollIndicator={false}
            renderItem={({item, index}) => (
              <TouchableOpacity
                onPress={() =>
                  Router.navigate('FormsCategoryItem', {
                    categoryName: item.categoryName,
                    subCategoryDTOs: item.subCategoryDTOs,
                  })
                }>
                <ElevatedView
                  elevation={2}
                  style={{
                    height: 100,
                    width: Dimensions.get('window').width * 0.98,
                    margin: '1%',
                    borderRadius: 8,
                    borderWidth: 0.5,
                    borderColor: '#f9f9f9',
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    backgroundColor: '#f9f9f9',
                  }}>
                  {index % 2 == 0 ? (
                    <View style={{height: '100%', width: '48%'}}>
                      <SvgComponent index={index} />
                    </View>
                  ) : (
                    <Text
                      style={{
                        fontFamily: 'Lato-Bold',
                        textTransform: 'uppercase',
                        height: '100%',
                        width: '48%',
                        textAlign: 'center',
                        textAlignVertical: 'center',
                        fontSize: 16,
                      }}>
                      {item.categoryName}
                    </Text>
                  )}
                  {index % 2 == 0 ? (
                    <Text
                      style={{
                        fontFamily: 'Lato-Bold',
                        textTransform: 'uppercase',
                        height: '100%',
                        width: '48%',
                        textAlign: 'center',
                        textAlignVertical: 'center',
                        fontSize: 16,
                      }}>
                      {item.categoryName}
                    </Text>
                  ) : (
                    <View style={{height: '100%', width: '48%'}}>
                      <SvgComponent index={index} />
                    </View>
                  )}
                </ElevatedView>
              </TouchableOpacity>
            )}
          />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
});
