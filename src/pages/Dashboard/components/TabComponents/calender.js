/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  Text,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {
  Card,
  ListItem,
  Thumbnail,
  Left,
  Body,
  Right,
  Button,
} from 'native-base';
import dayjs from 'dayjs';
import {Calendar} from 'react-native-calendars';
import DocumentPicker from 'react-native-document-picker';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {ScrollView} from 'react-native-gesture-handler';
import AuthSingleton from '../../../../utils/auth-singleton';
import Axios from 'axios';
import {Agenda} from 'react-native-calendars';
import typography from '../../../../styles/typography';

export default class CalendarTabComponent extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      EventData: undefined,
      items: {},
    };
  }

  async componentDidMount() {
    await this.getEventDataByCustomerId();
  }

  getEventDataByCustomerId() {
    var url =
      'http://onfiling1-env.eug9cevh2s.ap-south-1.elasticbeanstalk.com/onfiling/CalendarEvents/all';
    var authSingleton = new AuthSingleton();
    var userId = authSingleton.getUserID();
    var authToken = authSingleton.getAuthToken();

    Axios.get(url, {
      headers: {
        Authorization: 'Bearer ' + authToken,
        'Content-Type': 'application/json',
        clientid: userId,
      },
    })
      .then(response => {
        console.log('Response', response.data.object);
        this.setState({EventData: response.data.object});
        this.renderEventItem();
      })
      .catch(error => {
        console.log('Error', error);
      });
  }

  async renderEventItem() {
    for (var i = 0; i < this.state.EventData.length; i++) {
      var item = this.state.EventData[i];
      console.log('Item Event map', item);
      var start = new Date(item.eventFromDate);
      var sd = start.getTime();
      var end = new Date(item.eventToDate);
      var ed = end.getTime();
      let eventFromDate = this.dateString(item.eventFromDate);
      let eventToDate = this.dateString(item.eventToDate);
      console.log('eventFromDate', eventFromDate);
      console.log('eventToDate', eventToDate);
      if (this.state.items[eventFromDate].length == 0) {
        this.state.items[eventFromDate] = [];
        await this.state.items[eventFromDate].push({
          fullName: item.event,
          item: item,
        });
        console.log('not present in new date', this.state.items);
      } else {
        let length =
          this.state.items[eventFromDate] &&
          this.state.items[eventFromDate].length;
        for (var a = 0; a < length; a++) {
          if (this.state.items[eventFromDate][a].fullName == item.event) {
            console.log('Alredy present in new date');
          } else {
            await this.state.items[eventFromDate].push({
              fullName: item.event,
              item: item,
            });
            console.log('not present in new date', this.state.items);
          }
        }
      }
      if (eventFromDate === eventToDate) {
      } else {
        var diff = (ed - sd) / 86400000;
        for (var i = 1; i <= diff; i++) {
          const time = sd + i * 24 * 60 * 60 * 1000;
          const strTime = this.timeToString(time);
          if (
            this.state.items[strTime] &&
            this.state.items[strTime].length == 0
          ) {
            this.state.items[strTime] = [];
            await this.state.items[strTime].push({
              fullName: item.event,
              item: item,
            });
            console.log('not present in new date', this.state.items);
          } else {
            let length =
              this.state.items[strTime] && this.state.items[strTime].length;
            for (var a = 0; a < length; a++) {
              if (this.state.items[strTime][a].fullName === item.event) {
                console.log('Alredy present');
              } else {
                await this.state.items[strTime].push({
                  fullName: item.event,
                  item: item,
                });
                console.log('not present in new date', this.state.items);
              }
            }
          }
        }
      }
      const newItems = {};
      Object.keys(this.state.items).forEach(key => {
        newItems[key] = this.state.items[key];
      });
      this.setState({
        items: newItems,
      });
      console.log('items', this.state.items);
    }
  }

  dateString(d) {
    let date = new Date(d);
    return dayjs(date).format('YYYY-MM-DD');
  }

  loadItems(day) {
    for (let i = -15; i < 85; i++) {
      const t = day.timestamp + i * 24 * 60 * 60 * 1000;
      const tt = this.timeToString(t);
      if (!this.state.items[tt]) {
        this.state.items[tt] = [];
        const numItems = 0;
      }
    }
  }

  renderItem(item) {
    console.log('item', item);
    return (
      <TouchableOpacity
        style={[styles.item, {backgroundColor: item.item.color}]}>
        <Text style={[typography.body2, {color: 'white'}]}>
          {item.fullName}
        </Text>

        <Text style={[typography.caption, {color: 'white'}]}>
          {item.item.eventDescription}
        </Text>
      </TouchableOpacity>
    );
  }

  renderEmptyDate() {
    return (
      <View style={styles.emptyDate}>
        {/* <Text>{Nothing to show.}</Text> */}
      </View>
    );
  }

  rowHasChanged(r1, r2) {
    return r1.name !== r2.name;
  }

  timeToString(time) {
    const date = new Date(time);
    return date.toISOString().split('T')[0];
  }

  render() {
    let date = new Date();

    let newDate = dayjs(date).format('YYYY-MM-DD');
    return (
      <View style={{backgroundColor: 'white', flex: 1, alignItems: 'center'}}>
        <Agenda
          items={this.state.items}
          pastScrollRange={50}
          loadItemsForMonth={this.loadItems.bind(this)}
          selected={newDate}
          renderItem={this.renderItem.bind(this)}
          renderEmptyDate={this.renderEmptyDate.bind(this)}
          rowHasChanged={this.rowHasChanged.bind(this)}
          style={{width: Dimensions.get('screen').width}}
          // theme={{
          //   agendaDayTextColor: 'yellow',
          //   agendaDayNumColor: 'green',
          //   agendaTodayColor: 'green',
          //   agendaKnobColor: 'blue',
          // }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  item: {
    backgroundColor: 'white',
    flex: 1,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17,
  },
  emptyDate: {
    height: 15,
    flex: 1,
    paddingTop: 30,
  },
});
