/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Image,
  StyleSheet,
  Text,
  TextInput,
  ScrollView,
  FlatList,
} from 'react-native';
import ElevatedView from 'react-native-elevated-view';
import Layout from '../../../../styles/layout';
import Theme from '../../../../styles/theme';
import Typography from '../../../../styles/typography';
import Assets from '../../../../assets';
import Axios from 'axios';
import AuthSingleton from '../../../../utils/auth-singleton';
export default class SupportTabComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      chatData: [],
      data: [
        {
          id: '1',
          title: 'First Item',
        },
        {
          id: '5',
          title: 'Second Item',
        },
        {
          id: '6',
          title: 'Third Item',
        },
      ],
      review: '',
    };
  }

  componentDidMount() {
    this.getChatDataByCustomerId();
  }

  getChatDataByCustomerId() {
    var url =
      'http://onfiling1-env.eug9cevh2s.ap-south-1.elasticbeanstalk.com/onfiling/QueryChat/all';
    var authSingleton = new AuthSingleton();
    var userId = authSingleton.getUserID();
    var authToken = authSingleton.getAuthToken();

    Axios.get(url, {
      headers: {
        Authorization: 'Bearer ' + authToken,
        'Content-Type': 'application/json',
        clientId: userId,
      },
    })
      .then(response => {
        console.log('Response', response);
        this.setState({chatData: response.data.object});
      })
      .catch(error => {
        console.log('Error', error);
      });
  }

  render() {
    return (
      <View style={[Layout.safeArea, {marginLeft: 8, marginRight: 8}]}>
        <ScrollView
          style={[{marginTop: 4, marginHorizontal: 4}]}
          showsVerticalScrollIndicator={false}>
          <View style={[Layout.row]}>
            <ElevatedView
              elevation={0}
              style={[Layout.row, styles.searchBarContainer]}>
              <Image
                source={Assets.Wallpaper2}
                style={{
                  height: 40,
                  width: 40,
                  borderRadius: 20,
                  marginRight: 16,
                }}
              />
              <TextInput
                placeholder="Write Your Message"
                style={[styles.textInputSearch]}
                underlineColorAndroid={'transparent'}
                onChangeText={review => this.setState({review})}
                value={this.state.review}
              />
            </ElevatedView>
          </View>
          <FlatList
            horizontal={false}
            showsVerticalScrollIndicator={false}
            renderItem={() => (
              <ElevatedView
                elevation={0.5}
                style={[
                  Layout.row,
                  {
                    marginVertical: 1,
                    paddingVertical: 6,
                    width: '100%',
                    backgroundColor: 'white',
                  },
                ]}>
                <Image
                  source={Assets.Wallpaper2}
                  style={{
                    height: 40,
                    width: 40,
                    borderRadius: 20,
                    marginRight: 20,
                  }}
                />
                <View
                  style={[
                    Layout.row,
                    {justifyContent: 'space-between', width: '84%'},
                  ]}>
                  <View style={{width: '70%'}}>
                    <Text
                      style={[
                        Typography.body2,
                        {fontWeight: 'bold', marginBottom: 2},
                      ]}>
                      Sourabh
                    </Text>
                    <Text numberOfLines={2} style={[Typography.body]}>
                      Help in document upload
                    </Text>
                  </View>
                  <Text
                    style={[Typography.body, {color: Theme.colors.primary}]}>
                    4/11/19
                  </Text>
                </View>
              </ElevatedView>
            )}
            data={this.state.data}
            keyExtractor={(item, index) => item + index}
          />
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  searchBarContainer: {
    paddingVertical: 4,
    width: '100%',
    borderBottomColor: '#f0f0f0',
    borderBottomWidth: 1,
  },
  textInputSearch: {
    paddingLeft: 4,
    width: '90%',
  },
  textSearchButton: {
    marginBottom: 10,
    width: 48,
    borderRadius: 30,
    backgroundColor: Theme.colors.primary,
    height: 48,
  },
  ImageBackgroundStyle: {
    width: '100%',
    height: '100%',
  },
  containerStyle: {
    marginHorizontal: 12,
    marginVertical: 32,
    justifyContent: 'space-between',
  },
  column: {
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'flex-end',
  },
  contentCenter: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  pageColumnCentered: {
    backgroundColor: 'white',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  top: {
    marginVertical: 20,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
});
