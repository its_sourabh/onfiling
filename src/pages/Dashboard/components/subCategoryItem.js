import React from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  Dimensions,
  Text,
  Image,
  TouchableOpacity,
  Modal,
  Alert,
  Linking,
  TextInput,
} from 'react-native';
import {Card, Fab, Thumbnail} from 'native-base';
import {Header} from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import assets from '../../../assets/index';
import Router from '../../../RootNavigator/Router';
import PageLoader from '../../../common/components/PageLoader';
import Layout from '../../../styles/layout';
import {Button} from 'react-native-elements';
import Axios from 'axios';
import AuthSingleton from '../../../utils/auth-singleton';
import DocumentPicker from 'react-native-document-picker';
import Theme from '../../../styles/theme';
import Spacing from '../../../styles/spacing';
import Typography from '../../../styles/typography';
import dayjs from 'dayjs';
export default class SubCategoryItem extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      ProductListData: ['data', 'data'],
      data: [
        {
          id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
          title: 'First Item',
        },
        {
          id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
          title: 'Second Item',
        },
        {
          id: '58694a0f-3da1-471f-bd96-145571e29d72',
          title: 'Third Item',
        },
      ],
      documenData: [],
      selectedFiles: [],
      uploadModalVisible: false,
      isLoading: true,
      editTextOfIndex: -1,
    };
  }

  setUploadModalVisible(visible) {
    this.setState({uploadModalVisible: visible});
  }
  componentDidMount() {
    this.getDocumentBySubCategoryId(
      this.props.navigation.state.params.subCategoryId,
      this.props.navigation.state.params.categoryId,
    );
  }

  async _uploadAllDocumentToServer() {
    var allDocument = [];
    allDocument = allDocument.concat(this.state.selectedFiles);
    for (var i = allDocument.length; i > 0; i--) {
      console.log('Files selected', allDocument[i - 1], i);
      await this._uploadDocumentToServer(allDocument[i - 1], i - 1);
    }
    this.setState({selectedFiles: []});
    Alert.alert('ONFILING', 'Document Uploaded SuccessFully');
  }

  _uploadDocumentToServer(fileToBeUploaded, indexOfFile) {
    var authSingleton = new AuthSingleton();
    var userId = authSingleton.getUserID();
    var url =
      'http://onfiling1-env.eug9cevh2s.ap-south-1.elasticbeanstalk.com/onfiling/DocumentUpload/document/upload';

    var body = {
      categoryDTO: {id: this.props.navigation.state.params.categoryId},
      subCategoryDTO: {id: this.props.navigation.state.params.subCategoryId},
      clientRegistrationDTO: {id: userId},
      docName: fileToBeUploaded.name,
      docDescription: fileToBeUploaded.name,
      imageType: fileToBeUploaded.type,
      imageName: fileToBeUploaded.name,
      imageSize: fileToBeUploaded.size,
    };

    // console.log('body in upload section', body, documentUploadDTO);
    var form = new FormData();
    form.append('documentUploadDTO', JSON.stringify(body));
    form.append('file', fileToBeUploaded);

    Axios.post(url, form)
      .then(response => {
        console.log('Response on Upload', response);
        this.componentDidMount();
      })
      .catch(error => {
        console.log('Error', error);
      });
  }

  _uploadDocument = async () => {
    try {
      const results = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.images],
      });
      this.setState({selectedFiles: results});
      console.log(results);
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  };

  deleteItem(selectedItem, item) {
    console.log('insied sadasd Item', selectedItem[0]);
    console.log('insied item Item', item);
    var filesUpload = selectedItem;
    var len = filesUpload.length;
    console.log('lenth ', len);

    for (var i = 0; i < len; i++) {
      console.log('Iteration', i);
      if (filesUpload[i].name == item.name) {
        //console.log("mAtch");
        filesUpload.pop(filesUpload[i]);
        console.log('After pop Selected Item ', filesUpload);
        break;
      } else {
        //console.log("Not mAtch");
        // newArr.push(selectedItem[i])
      }
    }
    this.setState({selectedFiles: filesUpload});
    this.forceUpdate();
  }

  getDocumentBySubCategoryId(subCategoryId, categoryId) {
    var authSingleton = new AuthSingleton();
    var userId = authSingleton.getUserID();
    var authToken = authSingleton.getAuthToken();
    var url =
      'http://onfiling1-env.eug9cevh2s.ap-south-1.elasticbeanstalk.com/onfiling/DocumentUpload/all';
    Axios.get(url, {
      headers: {
        subCategoryId: subCategoryId,
        categoryId: categoryId,
        clientId: userId,
        Authorization: 'Bearer ' + authToken,
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        console.log('Response', response);
        this.setState({documenData: response.data.object, isLoading: false});
      })
      .catch(error => {
        console.log('Error', error);
        this.setState({isLoading: false});
      });
  }

  editSelecteditem(item, index) {
    this.setState({editTextOfIndex: index});
  }

  render() {
    console.log('Props in the subCategory', this.props);
    const deviceWidth = Dimensions.get('window').width;
    return (
      <View style={[styles.safeArea]}>
        <View style={[styles.pageColumn, styles.cardContainer]}>
          <View
            style={[
              {
                backgroundColor: 'white',
                height: 54,
                borderBottomWidth: 0,
                flexDirection: 'row',
                alignItems: 'center',
                paddingLeft: 8,
                borderBottomWidth: 1,
                borderColor: '#f1f1f1',
                width: '100%',
              },
            ]}>
            <Icon
              name="arrow-left"
              color={'#ffb600'}
              size={30}
              onPress={() => Router.back()}
            />
            <View
              style={{
                width: '80%',
                marginLeft: 15,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                numberOfLines={1}
                style={{
                  fontSize: 18,
                  color: 'black',
                  fontWeight: 'bold',
                  textAlign: 'justify',
                }}>
                {'Documents'}
              </Text>
            </View>
          </View>
          {this.state.isLoading ? (
            <PageLoader />
          ) : this.state.documenData.length == 0 ||
            this.state.documenData.length < 0 ? (
            <View
              style={{
                width: '100%',
                height: '100%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text>No Document Uploaded Yet</Text>
            </View>
          ) : (
            <FlatList
              data={this.state.documenData}
              numColumns={2}
              renderItem={({item, index}) => (
                <TouchableOpacity
                  onPress={() => {
                    Linking.openURL(item.image);
                  }}
                  noShadow
                  style={{
                    width: deviceWidth / 2 - 12,
                    height: 220,
                    backgroundColor: 'white',
                    borderRadius: 8,
                    justifyContent: 'space-around',
                    alignItems: 'center',
                    padding: 15,
                    margin: 5,
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <Image
                      style={{width: 80, height: 80}}
                      source={assets.docIcon}
                    />
                  </View>
                  <View style={{width: '100%', alignItems: 'center'}}>
                    <Text
                      numberOfLines={1}
                      style={{fontSize: 14, fontWeight: 'bold'}}>
                      {item.docName}
                    </Text>
                    <Text style={{fontSize: 10, color: 'grey'}}>
                      {dayjs(item.updatedOn).format('DD-MM-YYYY')} Created
                    </Text>
                  </View>
                </TouchableOpacity>
              )}
              keyExtractor={item => item.id}
            />
          )}
          <Fab
            direction="up"
            containerStyle={{}}
            style={{backgroundColor: '#ffb600'}}
            position="bottomRight"
            onPress={() => this.setUploadModalVisible(true)}>
            <Icon name="upload" />
          </Fab>
        </View>
        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.uploadModalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View
            style={[
              Layout.safeArea,
              {
                justifyContent: 'center',
                backgroundColor: Theme.colors.placeholder,
              },
            ]}>
            <View
              style={[
                Layout.selfCenter,
                {
                  paddingVertical: 14,
                  paddingHorizontal: 12,
                  width: '90%',
                  backgroundColor: 'white',
                  borderRadius: 16,
                },
              ]}>
              <View style={[styles.innerWrapper]}>
                <Text
                  style={[
                    Layout.selfStart,
                    Typography.title,
                    {paddingLeft: 12, fontWeight: 'bold'},
                  ]}>
                  UPLOAD
                </Text>

                <View style={{marginBottom: 25}}>
                  {this.state.selectedFiles.length == 0 ? (
                    <TouchableOpacity
                      style={[
                        Layout.contentCenter,
                        {
                          marginVertical: 16,
                          paddingHorizontal: 10,
                          paddingVertical: 20,
                          backgroundColor: '#cedce2',
                          borderWidth: 0.5,
                          borderStyle: 'dashed',
                          borderColor: Theme.colors.primary,
                        },
                      ]}
                      onPress={() => this._uploadDocument()}>
                      <Text
                        style={[
                          Typography.subheading,
                          {color: Theme.colors.primary},
                        ]}>
                        Select Files Here
                      </Text>
                    </TouchableOpacity>
                  ) : (
                    <FlatList
                      data={this.state.selectedFiles}
                      style={{width: '100%', maxHeight: 400}}
                      extraData={this.state.editTextOfIndex}
                      renderItem={({item, index}) => (
                        <View
                          style={[
                            Layout.row,
                            {justifyContent: 'space-between', marginBottom: 5},
                          ]}>
                          <Thumbnail square source={{uri: item.uri}} />
                          {this.state.editTextOfIndex == index ? (
                            <TextInput
                              style={{width: '65%'}}
                              value={item.name}
                              onChangeText={text => {
                                item.name = text;
                                var sd = [];
                                sd = sd.concat(this.state.selectedFiles);
                                sd[this.state.editTextOfIndex].name = text;
                                this.setState({selectedFiles: sd});
                              }}
                              placeholder={'Enter Doc Name'}
                            />
                          ) : (
                            <Text
                              onPress={() => this.editSelecteditem(item, index)}
                              style={{width: '65%'}}
                              numberOfLines={1}>
                              {item.name}
                            </Text>
                          )}

                          {this.state.editTextOfIndex == index ? (
                            <TouchableOpacity
                              style={{
                                height: 30,
                                width: 30,
                                justifyContent: 'center',
                                alignItems: 'center',
                                backgroundColor: Theme.colors.primary,
                                borderRadius: 50,
                              }}
                              onPress={() => {
                                this.setState({editTextOfIndex: -1});
                              }}>
                              <Icon name="check" size={20} color={'white'} />
                            </TouchableOpacity>
                          ) : (
                            <TouchableOpacity
                              style={{
                                height: 30,
                                width: 30,
                                justifyContent: 'center',
                                alignItems: 'center',
                              }}
                              onPress={() =>
                                this.deleteItem(this.state.selectedFiles, item)
                              }>
                              <Icon name="delete" size={20} color="black" />
                            </TouchableOpacity>
                          )}
                        </View>
                      )}
                      keyExtractor={item => item.id}
                    />
                  )}
                </View>
                <View
                  style={[
                    Layout.row,
                    {
                      width: '100%',
                      justifyContent: 'space-evenly',
                    },
                  ]}>
                  <View style={{width: '45%'}}>
                    <Button
                      title="CLOSE"
                      onPress={() => {
                        this.setUploadModalVisible(
                          !this.state.uploadModalVisible,
                        );
                      }}
                      titleStyle={[
                        Typography.subheading,
                        {
                          color: Theme.colors.primary,
                          fontWeight: 'bold',
                          fontSize: 14,
                        },
                      ]}
                      buttonStyle={{backgroundColor: 'transparent'}}
                      containerStyle={[
                        Layout.contentCenter,
                        styles.buttonStyle,
                      ]}
                    />
                  </View>
                  <View style={{width: '45%'}}>
                    <Button
                      onPress={() => this._uploadAllDocumentToServer()}
                      title="UPLOAD"
                      titleStyle={[
                        Typography.subheading,
                        {
                          color: Theme.colors.textLight,
                          fontWeight: 'bold',
                          fontSize: 14,
                        },
                      ]}
                      buttonStyle={{backgroundColor: 'transparent'}}
                      containerStyle={[
                        Layout.contentCenter,
                        styles.buttonStyle,
                        {
                          backgroundColor: Theme.colors.primary,
                        },
                      ]}
                    />
                  </View>
                </View>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  badgeContainerStyle4: {
    position: 'absolute',
  },
  badge: {
    backgroundColor: 'white',
    height: 25,
    width: 25,
    borderColor: '#ffb600',
    borderWidth: 2,
    borderRadius: 20,
  },
  badgeText: {
    color: 'black',
  },
  safeArea: {
    flex: 1,
    backgroundColor: 'white',
  },
  column: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
  },
  contentCenter: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  selfCenter: {
    alignSelf: 'center',
    textAlign: 'center',
  },
  pageColumnCentered: {
    marginHorizontal: 40,
    backgroundColor: 'white',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  top: {
    marginVertical: 40,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  IconStyle: {
    marginRight: 4,
  },
  ImageBackgroundStyle: {
    height: '94%',
    width: '94%',
    marginLeft: -4,
    borderRadius: 4,
  },
  listWrapper: {
    width: '98%',
    height: 296,
    marginRight: 2,
    marginLeft: 2,
    borderRadius: 5,
    marginVertical: 4,
  },
  textWrapper: {
    marginLeft: 8,
    marginBottom: 4,
  },
  cardContainer: {
    marginBottom: 1,
    paddingLeft: 4,
    paddingRight: 4,
    backgroundColor: '#F0F0F0',
  },
  pageColumn: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
  },

  badgeContainerStyle4: {
    position: 'absolute',
  },
  innerWrapper: {
    width: '100%',
    backgroundColor: 'white',
    padding: 10,
  },
  safeArea: {
    flex: 1,
    backgroundColor: 'white',
  },
  column: {
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
  },
  contentCenter: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  selfCenter: {
    alignSelf: 'center',
    textAlign: 'center',
  },
  pageColumnCentered: {
    marginHorizontal: 40,
    backgroundColor: 'white',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  top: {
    marginVertical: 40,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  IconStyle: {
    marginRight: 4,
  },
  ImageBackgroundStyle: {
    height: '100%',
    width: '98%',
    marginLeft: 0,
    borderRadius: 4,
    resizeMode: 'center',
  },
  listWrapper: {
    height: 200,
    marginRight: 2,
    marginLeft: 2,
    borderRadius: 5,
    marginVertical: 4,
  },
  textWrapper: {
    marginLeft: 8,
    marginBottom: 4,
  },
  cardContainer: {
    marginBottom: 4,
    paddingLeft: 4,
    paddingRight: 4,
    backgroundColor: '#F0F0F0',
  },
  pageColumn: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
  },
  buttonStyle: {
    backgroundColor: Theme.colors.surface,
    height: Spacing.height5,
    borderWidth: 1,
    borderColor: Theme.colors.primary,
    width: '100%',
    borderRadius: 5,
  },
});
