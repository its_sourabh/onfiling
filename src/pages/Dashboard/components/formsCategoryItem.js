import React from 'react';
import axios from 'axios';
import {
  View,
  StyleSheet,
  FlatList,
  Text,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  Image,
  Linking,
} from 'react-native';
import {Header} from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import AuthSingleton from '../../../utils/auth-singleton';
import Router from '../../../RootNavigator/Router';
import PageLoader from '../../../common/components/PageLoader';
import assets from '../../../assets/index';
import {Content, Accordion, Card} from 'native-base';
import typography from '../../../styles/typography';
import layout from '../../../styles/layout';
import ElevatedView from 'react-native-elevated-view';
import Axios from 'axios';
import theme from '../../../styles/theme';
export default class FormsCategoryItem extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        {
          code: 1,
          id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
          title: 'First Item',
          color: '#fff',
          iconColor: '#6bade3',
          image: 'circle-blue.png',
          data: [
            {
              id: '1',
              title: 'First Item',
              iconColor: '#6bade3',
            },
            {
              id: '2',
              title: 'Second Item',
              iconColor: '#6bade3',
            },
            {
              id: '3',
              title: 'Third Item',

              iconColor: '#6bade3',
            },
            {
              id: '4',
              title: 'First Item',

              iconColor: '#6bade3',
            },
            {
              id: '5',
              title: 'Second Item',
              iconColor: '#6bade3',
            },
            {
              id: '6',
              title: 'Third Item',
              iconColor: '#6bade3',
            },
            {
              id: '7',
              title: 'First Item',
              iconColor: '#6bade3',
            },
            {
              id: '8',
              title: 'Second Item',
              iconColor: '#6bade3',
            },
            {
              id: '9',
              title: 'Third Item',

              iconColor: '#6bade3',
            },
          ],
        },
        {
          code: 2,
          id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
          title: 'Second Item',
          color: '#6bade3',
          iconColor: '#fff',
          image: 'circle-white.png',
          data: [
            {
              id: '11',
              title: 'First Item',
              iconColor: '#fff',
            },
            {
              id: '12',
              title: 'Second Item',
              iconColor: '#fff',
            },
            {
              id: '13',
              title: 'Third Item',

              iconColor: '#fff',
            },
            {
              id: '14',
              title: 'First Item',

              iconColor: '#fff',
            },
            {
              id: '15',
              title: 'Second Item',
              iconColor: '#fff',
            },
            {
              id: '16',
              title: 'Third Item',

              iconColor: '#fff',
            },
            {
              id: '17',
              title: 'First Item',

              iconColor: '#fff',
            },
            {
              id: '18',
              title: 'Second Item',
              iconColor: '#fff',
            },
            {
              id: '19',
              title: 'Second Item',
              iconColor: '#fff',
            },
          ],
        },

        {
          code: 3,
          id: '58694a0f-3da1-471f-bd96-145571e29d72',
          title: 'Third Item',
          color: '#fff',
          iconColor: '#6bade3',
          image: 'circle-blue.png',
          data: [
            {
              id: '31',
              title: 'First Item',

              iconColor: '#6bade3',
            },
            {
              id: '32',
              title: 'Second Item',
              iconColor: '#6bade3',
            },
            {
              id: '33',
              title: 'Third Item',

              iconColor: '#6bade3',
            },
            {
              id: '34',
              title: 'First Item',

              iconColor: '#6bade3',
            },
            {
              id: '35',
              title: 'Second Item',
              iconColor: '#6bade3',
            },
            {
              id: '36',
              title: 'Third Item',
              iconColor: '#6bade3',
            },
            {
              id: '37',
              title: 'First Item',
              iconColor: '#6bade3',
            },
            {
              id: '38',
              title: 'Second Item',
              iconColor: '#6bade3',
            },
            {
              id: '39',
              title: 'Third Item',

              iconColor: '#6bade3',
            },
          ],
        },
      ],
    };
  }

  _renderHeader(item, expanded, index) {
    console.log('_renderHeader');
    return (
      <View
        style={{
          padding: 10,
          paddingBottom: 20,
          paddingRight: 20,
          paddingLeft: 20,
          backgroundColor: 'white',
          marginBottom: 4,
        }}>
        <View
          style={{
            marginTop: 16,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <Text
            style={[
              typography.subheading,
              {fontWeight: 'bold', color: 'black', fontSize: 18},
            ]}>
            {item.subCategoryName}
          </Text>
          {expanded ? (
            <Icon style={{fontSize: 30, color: 'black'}} name="minus" />
          ) : (
            <Icon style={{fontSize: 30, color: 'black'}} name="plus" />
          )}
        </View>
        {/* <Text
          style={[
            typography.caption,
            {color: 'white', fontSize: 10, marginTop: -6},
          ]}>
          {item.subCategoryDesc ? item.subCategoryDesc : 'NOT AVAILABLE'}
        </Text> */}
      </View>
    );
  }

  _renderContent(item) {
    return (
      <FlatList
        showsVerticalScrollIndicator={false}
        data={item.contentDTOs}
        style={{
          width: '100%',
          backgroundColor: '#f9f9f9',
          paddingBottom: 16,
          paddingTop: 12,
        }}
        numColumns={1}
        renderItem={({item, index}) => (
          <TouchableOpacity
            onPress={() =>
              Linking.openURL('https://www.onfiling.com/#/service?q=' + item.id)
            }
            style={{
              paddingLeft: 12,
              marginVertical: 4,
              height: 30,
            }}>
            <View style={{flexDirection: 'row', width: '100%', height: '80%'}}>
              <Icon
                name="chevron-right"
                size={20}
                style={{color: 'black', paddingRight: 8}}
              />
              <Text style={[typography.body, {color: 'black'}]}>
                {item.contentName}
              </Text>
            </View>
            <View
              style={{
                borderWidth: 0.5,
                width: '90%',
                height: 1,
                alignSelf: 'center',
                backgroundColor: '#f1f1f1',
                borderColor: '#e1e1e1',
              }}
            />
          </TouchableOpacity>
        )}
        keyExtractor={item => item.id}
      />
    );
  }

  render() {
    const deviceWidth = Dimensions.get('window').width;
    console.log(
      'params in categoryItem',
      this.props.navigation.state.params.categoryId,
    );
    const naviagtedData = this.props.navigation.state.params;
    return (
      <View style={[styles.safeArea]}>
        <View style={[styles.pageColumn, styles.cardContainer]}>
          <View
            style={[
              {
                backgroundColor: 'white',
                height: 54,
                borderBottomWidth: 0,
                flexDirection: 'row',
                alignItems: 'center',
                paddingLeft: 8,
                borderBottomWidth: 1,
                borderColor: '#f1f1f1',
                width: '100%',
              },
            ]}>
            <Icon
              name="arrow-left"
              color={'#ffb600'}
              size={30}
              onPress={() => Router.back()}
            />
            <View
              style={{
                width: '80%',
                marginLeft: 15,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                numberOfLines={1}
                style={{
                  fontSize: 18,
                  color: 'black',
                  fontWeight: 'bold',
                  textAlign: 'justify',
                  textTransform: 'capitalize',
                }}>
                {naviagtedData.categoryName}
              </Text>
            </View>
          </View>
          <Content style={{backgroundColor: '#f1f4f6', width: '100%'}}>
            <Accordion
              showsVerticalScrollIndicator={false}
              dataArray={naviagtedData.subCategoryDTOs}
              animation={true}
              expanded={true}
              style={{borderColor: 'transparent'}}
              renderHeader={this._renderHeader}
              renderContent={this._renderContent}
            />
          </Content>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  badgeContainerStyle4: {
    position: 'absolute',
  },
  badge: {
    backgroundColor: 'white',
    height: 25,
    width: 25,
    borderColor: '#2196F3',
    borderWidth: 2,
    borderRadius: 20,
  },
  badgeText: {
    color: 'black',
  },
  safeArea: {
    flex: 1,
    backgroundColor: 'white',
  },
  column: {
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
  },
  contentCenter: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  selfCenter: {
    alignSelf: 'center',
    textAlign: 'center',
  },
  pageColumnCentered: {
    marginHorizontal: 40,
    backgroundColor: 'white',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  top: {
    marginVertical: 40,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  IconStyle: {
    marginRight: 4,
  },
  ImageBackgroundStyle: {
    height: '100%',
    width: '98%',
    marginLeft: 0,
    borderRadius: 4,
    resizeMode: 'center',
  },
  listWrapper: {
    height: 200,
    marginRight: 2,
    marginLeft: 2,
    borderRadius: 5,
    marginVertical: 4,
  },
  textWrapper: {
    marginLeft: 8,
    marginBottom: 4,
  },
  cardContainer: {
    marginBottom: 1,
    paddingLeft: 4,
    paddingRight: 4,
    backgroundColor: '#F0F0F0',
  },
  pageColumn: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
  },
});
