import React from 'react';
import axios from 'axios';
import {
  View,
  StyleSheet,
  FlatList,
  Text,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  Image,
} from 'react-native';
import {Card} from 'native-base';
import {Header} from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import AuthSingleton from '../../../utils/auth-singleton';
import Router from '../../../RootNavigator/Router';
import PageLoader from '../../../common/components/PageLoader';
import assets from '../../../assets/index';
import layout from '../../../styles/layout';
import ElevatedView from 'react-native-elevated-view';
import Axios from 'axios';
export default class CategoryItem extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      ProductListData: ['data', 'data'],
      subCatgoryData: [],
    };
  }

  getSubCategoryDataFromServer() {
    var categoryId = this.props.navigation.state.params.categoryId;
    var url =
      'http://onfiling1-env.eug9cevh2s.ap-south-1.elasticbeanstalk.com/onfiling/SubCategory/single';

    var authSingleton = new AuthSingleton();
    var authToken = authSingleton.getAuthToken();
    var userId = authSingleton.getUserID();

    var self = this;
    Axios.get(url, {
      headers: {
        Authorization: 'Bearer ' + authToken,
        'Content-Type': 'application/json',
        clientId: userId,
        categoryId: categoryId,
      },
    })
      .then(function(response) {
        // self.setState({categoryData: response.data.object});
        console.log('Category Data Response From the Server', response);
        self.setState({subCatgoryData: response.data.object});
      })
      .catch(function(error) {
        console.log(error.response);
      });
  }

  componentDidMount() {
    this.getSubCategoryDataFromServer();
  }

  render() {
    const deviceWidth = Dimensions.get('window').width;
    console.log(
      'params in categoryItem',
      this.props.navigation.state.params.categoryId,
    );
    const naviagtedData = this.props.navigation.state.params;
    return (
      <View style={[styles.safeArea]}>
        {this.state.subCatgoryData == 0 ||
        this.state.subCatgoryData.length < 0 ? (
          <PageLoader />
        ) : (
          <View style={[styles.pageColumn, styles.cardContainer]}>
            <View
              style={[
                {
                  backgroundColor: 'white',
                  height: 54,
                  borderBottomWidth: 0,
                  flexDirection: 'row',
                  alignItems: 'center',
                  paddingLeft: 8,
                  borderBottomWidth: 1,
                  borderColor: '#f1f1f1',
                  width: '100%',
                },
              ]}>
              <Icon
                name="arrow-left"
                color={'#ffb600'}
                size={30}
                onPress={() => Router.navigate('Dashboard')}
              />
              <View
                style={{
                  width: '80%',
                  marginLeft: 15,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  numberOfLines={1}
                  style={{
                    fontSize: 18,
                    color: 'black',
                    fontWeight: 'bold',
                    textAlign: 'justify',
                  }}>
                  {naviagtedData.categoryName}
                </Text>
              </View>
            </View>

            {this.state.subCatgoryData.length != 0 ? (
              <FlatList
                data={this.state.subCatgoryData}
                numColumns={2}
                renderItem={item => (
                  <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={() =>
                      Router.navigate('SubCategoryItem', {
                        subCategoryId: item.item.id,
                        categoryId: this.props.navigation.state.params
                          .categoryId,
                      })
                    }>
                    <Card
                      noShadow
                      style={{
                        width: deviceWidth / 2 - 12,
                        height: 120,
                        backgroundColor: 'white',
                        borderRadius: 8,
                        justifyContent: 'space-around',
                        padding: 15,
                        margin: 5,
                      }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                        }}>
                        <Image
                          style={{width: 32, height: 32}}
                          source={assets.folderIcon}
                        />
                        <View>
                          <Text
                            style={{
                              backgroundColor: '#e7e7e7',
                              color: '#989898',
                              padding: 5,
                              fontWeight: 'bold',
                              borderRadius: 12,
                            }}>
                            {item.item.documentUploadDTOs.length}
                          </Text>
                        </View>
                      </View>
                      <View>
                        <Text
                          numberOfLines={1}
                          style={{fontSize: 14, fontWeight: 'bold'}}>
                          {item.item.subCategoryName}
                        </Text>
                        <Text style={{fontSize: 10, color: 'grey'}}>
                          {
                            (dd = new Date(
                              item.item.updatedOn,
                            ).toLocaleString())
                          }
                          {'\n' + '\n'}
                          Updated/Created
                        </Text>
                      </View>
                    </Card>
                  </TouchableOpacity>
                )}
                keyExtractor={item => item.id}
              />
            ) : (
              <View />
            )}
          </View>
        )}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  badgeContainerStyle4: {
    position: 'absolute',
  },
  badge: {
    backgroundColor: 'white',
    height: 25,
    width: 25,
    borderColor: '#2196F3',
    borderWidth: 2,
    borderRadius: 20,
  },
  badgeText: {
    color: 'black',
  },
  safeArea: {
    flex: 1,
    backgroundColor: 'white',
  },
  column: {
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
  },
  contentCenter: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  selfCenter: {
    alignSelf: 'center',
    textAlign: 'center',
  },
  pageColumnCentered: {
    marginHorizontal: 40,
    backgroundColor: 'white',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  top: {
    marginVertical: 40,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  IconStyle: {
    marginRight: 4,
  },
  ImageBackgroundStyle: {
    height: '100%',
    width: '98%',
    marginLeft: 0,
    borderRadius: 4,
    resizeMode: 'center',
  },
  listWrapper: {
    height: 200,
    marginRight: 2,
    marginLeft: 2,
    borderRadius: 5,
    marginVertical: 4,
  },
  textWrapper: {
    marginLeft: 8,
    marginBottom: 4,
  },
  cardContainer: {
    marginBottom: 1,
    paddingLeft: 4,
    paddingRight: 4,
    backgroundColor: '#F0F0F0',
  },
  pageColumn: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
  },
});
