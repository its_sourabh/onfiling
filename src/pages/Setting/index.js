import React from 'react';
import axios from 'axios';
import {
  View,
  StyleSheet,
  Text,
  Modal,
  Image,
  TouchableOpacity,
  AsyncStorage,
  TextInput,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Router from '../../RootNavigator/Router';
import {Button} from 'react-native-elements';
import PageLoader from '../../common/components/PageLoader';
import AuthSingleton from '../../utils/auth-singleton';
import Layout from '../../styles/layout';
import Typography from '../../styles/typography';
import Theme from '../../styles/theme';
import Assets from '../../assets';
import ElevatedView from 'react-native-elevated-view';
export default class CategoryIteam extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ProfileData: [],
      editProfile: false,
      updatedClientName: '',
      updatedClientMobileNo: '',
      updatedCompanyAddress: '',
      updatedCompanyMobileNo: '',
      updatedCompanyWebsite: '',
    };
    this.GetProfileDataFromServer = this.GetProfileDataFromServer.bind(this);
  }
  GetProfileDataFromServer = async () => {
    const authSingleton = new AuthSingleton();
    authSingleton.loadAuthToken();
    const authToken = authSingleton.getAuthToken();
    const userId = authSingleton.getUserID();
    console.log('authToken', authToken);
    let response = await axios.get(
      'http://onfiling1-env.eug9cevh2s.ap-south-1.elasticbeanstalk.com/onfiling/ClientRegistration/all',
      {
        headers: {
          Authorization: 'Bearer ' + authToken,
          'Content-Type': 'application/json',
          id: userId,
        },
      },
    );
    console.log('response', response);
    this.setState({
      ProfileData: response.data.object[0],
    });
    console.log('ProfileData in ', this.state.ProfileData);
  };
  componentDidMount() {
    this.GetProfileDataFromServer();
  }

  updateProfileDataToServer() {
    var url =
      'http://onfiling1-env.eug9cevh2s.ap-south-1.elasticbeanstalk.com/onfiling/ClientRegistration/createOrUpdate';

    var body = {
      clientName:
        this.state.updatedClientName == ''
          ? this.state.ProfileData.clientName
          : this.state.updatedClientName,
      clientPhoneNo:
        this.state.updatedClientMobileNo == ''
          ? this.state.ProfileData.clientPhoneNo
          : this.state.updatedClientName,
      companyAddress:
        this.state.updatedCompanyAddress == ''
          ? this.state.ProfileData.companyAddress
          : this.state.updatedCompanyAddress,
      companyPhoneNo:
        this.state.updatedCompanyMobileNo == ''
          ? this.state.ProfileData.companyPhoneNo
          : this.state.updatedCompanyMobileNo,
      companyWebsite:
        this.state.updatedCompanyWebsite == ''
          ? this.state.ProfileData.companyWebsite
          : this.state.updatedCompanyWebsite,
      id: this.state.ProfileData.id,
      clientEmailId: this.state.ProfileData.clientEmailId,
      companyName: this.state.ProfileData.companyName,
      formType: this.state.ProfileData.formType,
      gstNumber: this.state.ProfileData.gstNumber,
      password: this.state.ProfileData.password,
      selectedPackageDTO: null,
    };

    const authSingleton = new AuthSingleton();
    authSingleton.loadAuthToken();
    const authToken = authSingleton.getAuthToken();
    const userId = authSingleton.getUserID();

    axios
      .post(url, body, {
        headers: {
          Authorization: 'Bearer ' + authToken,
          'Content-Type': 'application/json',
          id: userId,
        },
      })
      .then(response => {
        console.log('Update Profile', response);
        this.setState({
          clientName: '',
          clientPhoneNo: '',
          companyAddress: '',
          companyPhoneNo: '',
          companyWebsite: '',
        });
        this.GetProfileDataFromServer();
      })
      .catch(error => {
        this.GetProfileDataFromServer();
      });

    // console.log('updateProfileDataToServer', body);
    this.setState({editProfile: false});
  }

  async logout() {
    // console.log('in logout');

    // Router.navigate('LogoutScreen');
    // new authSingleton
    await AsyncStorage.clear();
    // RNRestart.Restart();
    Router.navigate('SplashScreen');
    Router.navigate('SignIn');

    // await this.authSingleton.loadAuthToken();
    // await this.authSingleton.loadRefreshToken();
    // await this.authSingleton.loadUserId();
    // const authToken = await this.authSingleton.getAuthToken();
    // console.log('auth token', authToken);
  }

  render() {
    return (
      <View style={[styles.safeArea]}>
        {this.state.ProfileData == 0 || this.state.ProfileData < 0 ? (
          <PageLoader />
        ) : (
          <ScrollView style={[styles.safeArea, {backgroundColor: '#F0F0F0'}]}>
            <View
              style={[
                styles.row,
                {
                  justifyContent: 'space-between',
                  marginVertical: 12,
                  marginTop: 24,
                  marginHorizontal: 8,
                },
              ]}>
              <View style={[{marginLeft: 12, width: '64%'}]}>
                <Text
                  style={[
                    {
                      fontSize: 32,
                      fontWeight: 'bold',
                      color: 'black',
                    },
                  ]}>
                  {this.state.editProfile ? 'UPDATE PROFILE' : 'PROFILE'}
                </Text>
                <Text
                  style={[{fontSize: 8, fontWeight: 'bold', color: 'black'}]}>
                  CLICK ON TABS TO UPDATE VALUES
                </Text>
              </View>
              <ElevatedView
                elevation={2}
                style={[Layout.contentCenter, styles.iconContainer]}>
                <Icon
                  name={this.state.editProfile ? 'check' : 'power'}
                  color={Theme.colors.primary}
                  size={25}
                  onPress={() => {
                    this.state.editProfile
                      ? this.updateProfileDataToServer()
                      : this.logout();
                  }}
                />
              </ElevatedView>
            </View>
            <View style={[{marginTop: 8, marginHorizontal: 6}]}>
              <ElevatedView
                style={[
                  styles.row,
                  styles.contentCenter,
                  {
                    borderBottomColor: Theme.colors.disabled,
                    borderBottomWidth: 0.3,
                    backgroundColor: 'white',
                    height: 100,
                  },
                ]}>
                <View
                  style={[
                    styles.contentCenter,
                    {width: '24%', height: '100%'},
                  ]}>
                  <Image
                    source={Assets.Wallpaper3}
                    style={{height: '70%', width: '80%', borderRadius: 40}}
                  />
                </View>
                <View
                  style={[
                    styles.contentCenter,

                    {width: '76%', height: '100%'},
                  ]}>
                  <View style={{width: '100%', flexDirection: 'row'}}>
                    {this.state.editProfile ? (
                      <TextInput
                        value={this.state.updatedClientName}
                        onChangeText={text =>
                          this.setState({updatedClientName: text})
                        }
                        style={[
                          Layout.selfStart,
                          Typography.title,
                          {width: '70%'},
                        ]}
                        placeholder={'Enter Name'}
                      />
                    ) : (
                      <Text style={[Layout.selfStart, Typography.title]}>
                        {this.state.ProfileData.clientName}
                      </Text>
                    )}

                    {this.state.editProfile ? (
                      <View />
                    ) : (
                      <Icon
                        name="pencil"
                        color={Theme.colors.primary}
                        size={20}
                        style={{marginLeft: 10}}
                        onPress={() => {
                          this.setState({editProfile: !this.state.editProfile});
                        }}
                      />
                    )}
                  </View>

                  <Text
                    numberOfLines={2}
                    style={[Layout.selfStart, Typography.caption]}>
                    {this.state.ProfileData.clientEmailId}
                  </Text>
                </View>
              </ElevatedView>
              <ElevatedView
                style={[
                  {
                    backgroundColor: 'white',
                    paddingLeft: 20,
                    paddingTop: 16,
                    paddingBottom: 16,
                  },
                ]}>
                <Text style={[Typography.body, {fontWeight: 'bold'}]}>
                  ACCOUNT
                </Text>
                <Text
                  style={[Typography.caption, {textTransform: 'uppercase'}]}>
                  SIGNED IN For {this.state.ProfileData.companyName}
                </Text>
              </ElevatedView>
              <ElevatedView
                style={[
                  styles.row,
                  {
                    backgroundColor: 'white',
                    paddingVertical: 16,
                    borderBottomColor: Theme.colors.disabled,
                    borderBottomWidth: 0.3,
                  },
                ]}>
                <View style={[styles.contentCenter, {width: '15%'}]}>
                  <Icon name="phone" size={22} color={Theme.colors.primary} />
                </View>
                <View style={[{width: '85%'}]}>
                  <Text style={[Typography.body, {fontWeight: 'bold'}]}>
                    User's Mobile number
                  </Text>

                  {this.state.editProfile ? (
                    <TextInput
                      value={this.state.updatedClientMobileNo}
                      onChangeText={text =>
                        this.setState({updatedClientMobileNo: text})
                      }
                      keyboardType={'numeric'}
                      style={[
                        Typography.body,
                        {color: Theme.colors.placeholder},
                      ]}
                      placeholder="Enter Mobile Number"
                    />
                  ) : (
                    <Text
                      style={[
                        Typography.body,
                        {color: Theme.colors.placeholder},
                      ]}>
                      {this.state.ProfileData.clientPhoneNo}
                    </Text>
                  )}
                </View>
              </ElevatedView>

              <ElevatedView
                style={[
                  {
                    backgroundColor: 'white',
                    paddingLeft: 20,
                    paddingTop: 16,
                    paddingBottom: 16,
                  },
                ]}>
                <Text style={[Typography.body, {fontWeight: 'bold'}]}>
                  COMPANY'S DETAILS
                </Text>
              </ElevatedView>

              <ElevatedView
                style={[
                  styles.row,
                  {
                    backgroundColor: 'white',
                    paddingVertical: 16,
                  },
                ]}>
                <View style={[styles.contentCenter, {width: '15%'}]}>
                  <Icon name="domain" size={22} color={Theme.colors.primary} />
                </View>
                <View style={[{width: '85%'}]}>
                  <Text style={[Typography.body, {fontWeight: 'bold'}]}>
                    Company Name
                  </Text>
                  <Text
                    style={[
                      Typography.body,
                      {color: Theme.colors.placeholder},
                    ]}>
                    {this.state.ProfileData.companyName}
                  </Text>
                </View>
              </ElevatedView>

              <ElevatedView
                style={[
                  styles.row,
                  {
                    backgroundColor: 'white',
                    paddingVertical: 16,
                  },
                ]}>
                <View style={[styles.contentCenter, {width: '15%'}]}>
                  <Icon name="city" size={22} color={Theme.colors.primary} />
                </View>
                <View style={[{width: '85%'}]}>
                  <Text style={[Typography.body, {fontWeight: 'bold'}]}>
                    Company Address
                  </Text>
                  {this.state.editProfile ? (
                    <TextInput
                      value={this.state.updatedCompanyAddress}
                      onChangeText={text =>
                        this.setState({updatedCompanyAddress: text})
                      }
                      placeholder={'Enter Company Address'}
                      style={[
                        Typography.body,
                        {color: Theme.colors.placeholder},
                      ]}
                    />
                  ) : (
                    <Text
                      style={[
                        Typography.body,
                        {color: Theme.colors.placeholder},
                      ]}>
                      {this.state.ProfileData.companyAddress}
                    </Text>
                  )}
                </View>
              </ElevatedView>
              <ElevatedView
                style={[
                  styles.row,
                  {
                    backgroundColor: 'white',
                    paddingVertical: 16,
                  },
                ]}>
                <View style={[styles.contentCenter, {width: '15%'}]}>
                  <Icon
                    name="phone-classic"
                    size={22}
                    color={Theme.colors.primary}
                  />
                </View>
                <View style={[{width: '85%'}]}>
                  <Text style={[Typography.body, {fontWeight: 'bold'}]}>
                    Company Phone No.
                  </Text>
                  {this.state.editProfile ? (
                    <TextInput
                      value={this.state.updatedCompanyMobileNo}
                      onChangeText={text =>
                        this.setState({updatedCompanyMobileNo: text})
                      }
                      style={[
                        Typography.body,
                        {color: Theme.colors.placeholder},
                      ]}
                      keyboardType={'numeric'}
                      placeholder={'Enter Company Phone No.'}
                    />
                  ) : (
                    <Text
                      style={[
                        Typography.body,
                        {color: Theme.colors.placeholder},
                      ]}>
                      {this.state.ProfileData.companyPhoneNo}
                    </Text>
                  )}
                </View>
              </ElevatedView>

              <ElevatedView
                style={[
                  styles.row,
                  {
                    backgroundColor: 'white',
                    paddingVertical: 16,
                  },
                ]}>
                <View style={[styles.contentCenter, {width: '15%'}]}>
                  <Icon name="web" size={22} color={Theme.colors.primary} />
                </View>
                <View style={[{width: '85%'}]}>
                  <Text style={[Typography.body, {fontWeight: 'bold'}]}>
                    Company WebSite
                  </Text>
                  {this.state.editProfile ? (
                    <TextInput
                      value={this.state.updatedCompanyWebsite}
                      onChangeText={text =>
                        this.setState({updatedCompanyWebsite: text})
                      }
                      style={[
                        Typography.body,
                        {color: Theme.colors.placeholder},
                      ]}
                      placeholder={'Enter Website Url'}
                    />
                  ) : (
                    <Text
                      style={[
                        Typography.body,
                        {color: Theme.colors.placeholder},
                      ]}>
                      {this.state.ProfileData.companyWebsite}
                    </Text>
                  )}
                </View>
              </ElevatedView>
            </View>
          </ScrollView>
        )}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: 'white',
  },
  column: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
  },
  contentCenter: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  selfCenter: {
    alignSelf: 'center',
    textAlign: 'center',
  },
  selfEnd: {
    alignSelf: 'flex-end',
    textAlign: 'center',
  },
  pageColumnCentered: {
    marginHorizontal: 40,
    backgroundColor: 'white',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  top: {
    marginVertical: 40,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  imageStyle: {
    resizeMode: 'center',
    height: 18,
    width: 20,
    marginRight: 4,
  },
  ImageBackgroundStyle: {
    height: '70%',
    width: '100%',
    overflow: 'hidden',
    margin: 3,
  },
  textWrapper: {
    marginLeft: 8,
    marginBottom: 4,
  },
  iconContainer: {
    backgroundColor: Theme.colors.surface,
    borderRadius: 30,
    width: 44,
    height: 44,
    marginRight: 12,
  },
});
