import axios from 'axios';
import EventEmitter from '../../utils/event-emitter';
  const EVENT_TYPES = {
    REGISTER_START: 'REGISTER_START',
    REGISTER_SUCCESS: 'REGISTER_SUCCESS',
    REGISTER_FAILURE: 'REGISTER_FAILURE',  
  }
class RegisterUseCase {

  eventEmitter = null;
  constructor() {
    this.eventEmitter = new EventEmitter();
  }

  toString() {
    return 'RegisterUseCase';
  }

  getObservable() {
    return this.eventEmitter.getObservable();
  }

    execute = async (requestObject) => {
        if (!requestObject.phoneNumber) {
            this.eventEmitter.emit({
            type: EVENT_TYPES.REGISTER_FAILURE,
            data: 'Phone Number is required.' // This should an object of Granite Error here
            });
            return;
        }  
        // try {
        //   this.eventEmitter.emit({
        //     type: EVENT_TYPES.REGISTER_START,
        //   });
        //   const response = await axios.post('http://13.126.234.28/api/auth/user/create', {
        //         firstName: requestObject.firstName,
        //         emailId: requestObject.email,
        //         password: requestObject.password,
        //         contactNo: requestObject.phoneNumber,
        //         customAddress: '1',
        //         supplierId: '1'
        //       })
        //   console.log('response',response);   
        //   this.eventEmitter.emit({
        //     type: EVENT_TYPES.REGISTER_SUCCESS,
        //     data: response
        //   }); 
        //  } catch (e) {
        //   this.eventEmitter.emit({
        //     type: EVENT_TYPES.REGISTER_FAILURE,
        //     data: e.response.data.ErrorInfo.Message
        //   });
        //   return;
        // }  
        this.eventEmitter.emit({
            type: EVENT_TYPES.REGISTER_SUCCESS,
            data: response
        }); 
    }
}
export default RegisterUseCase;
