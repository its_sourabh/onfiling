import React, { Component } from 'react';
import { View } from 'react-native';

export default class NewProfile extends Component {
  render() {
    return (
      // Try setting `flexDirection` to `column`.
      <View style={{ flex: 1, backgroundColor: 'red' }}>
        <View style={{ flex: 0.6, backgroundColor: 'blue' }}>
          <Image
            style={{ width: '100%', height: '100%' }}
            source={{
              uri:
                'https://images.pexels.com/photos/1763075/pexels-photo-1763075.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260'
            }}
          />
        </View>
        <View style={{ flex: 1 }}></View>
      </View>
    );
  }
}
