import React from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  ScrollView,
  Text,
  ImageBackground,
  Image,
  TextInput,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Router from '../../../RootNavigator/Router';
import {Button, CheckBox, ThemeConsumer} from 'react-native-elements';
import Spacing from '../../../styles/spacing';
import Layout from '../../../styles/layout';
import Typography from '../../../styles/typography';
import Theme from '../../../styles/theme';
import Assets from '../../../assets';
import ElevatedView from 'react-native-elevated-view';
export default class CategoryIteam extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: false,
      checked2: false,
    };
  }

  render() {
    return (
      <View style={[Layout.safeArea, {backgroundColor: '#F0F0F0'}]}>
        <View
          style={[
            Layout.row,
            {
              justifyContent: 'space-between',
              marginVertical: 12,
              marginTop: 24,
              marginHorizontal: 8,
            },
          ]}>
          <View style={[{marginLeft: 12}]}>
            <Text style={[{fontSize: 32, fontWeight: 'bold', color: 'black'}]}>
              MOBILE
            </Text>
            <Text style={[{fontSize: 8, fontWeight: 'bold', color: 'black'}]}>
              UPDATE MOBILE NUMBER
            </Text>
          </View>
          <View>
            <Image
              source={Assets.close}
              onPress={() => Router.navigate('Setting')}
              style={{height: 42, width: 42, marginRight: 8}}
            />
          </View>
        </View>
        <View style={[Layout.contentCenter, Layout.verticalMargin]}>
          <Text
            style={[
              Typography.subheading,
              Layout.contentCenter,
              Layout.verticalMargin,
            ]}>
            Link active mobile number with your{'\n'}account
          </Text>
          <View style={[{marginTop: 16}]}>
            <TextInput
              style={[
                Typography.body,
                styles.inputFieldWrapper,
                styles.TextInputStyle,
              ]}
              placeholder="Mobile number"
              autoCorrect={false}
              underlineColorAndroid="transparent"
              onChangeText={text => this.setState({phoneNumber: text})}
              value={this.state.phoneNumber}
              clearButtonMode="while-editing"
              enablesReturnKeyAutomatically
              keyboardType="numeric"
              returnKeyType="next"
            />
          </View>
          <Button
            title="VERIFY"
            onPress={() => this.registerUsecase.execute(this.state)}
            buttonStyle={{borderRadius: 30, backgroundColor: '#ffb600'}}
            loading={this.state.isLoading}
            containerStyle={{
              height: 48,
              width: 260,
              borderRadius: 30,
              backgroundColor: '#ffb600',
              margin: 10,
            }}
          />
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  inputFieldWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: 0,
    marginBottom: 14,
    backgroundColor: 'white',
  },
  TextInputStyle: {
    width: 300,
    height: 48,
    marginBottom: 0,
    borderBottomWidth: 0,
  },
});
