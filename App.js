import React, {Component} from 'react';
import {Root} from 'native-base';
import RootNavigator from './src/RootNavigator/root';
import Router from './src/RootNavigator/Router';
import {MenuProvider} from 'react-native-popup-menu';
import {Container} from 'native-base';
import {StatusBar} from 'react-native';
export default class App extends Component {
  render() {
    return (
      <Root>
        <Container>
          <StatusBar hidden={true} />
          <MenuProvider>
            <RootNavigator
              ref={navigatorRef => {
                Router.setTopLevelNavigator(navigatorRef);
              }}
            />
          </MenuProvider>
        </Container>
      </Root>
    );
  }
}
